#include "TouchFileCommand.h"

namespace PSBook { namespace Commands
{

    SetFileTouchTimeCommand::SetFileTouchTimeCommand(void)
    {
        Date = DateTime::Now;
        rm = gcnew Resources::ResourceManager(L"PowershellCpp.Messages", GetType()->Assembly);
    }

    void SetFileTouchTimeCommand::ProcessRecord(void)
    {
        if (FileInfo != nullptr)
        {
            TouchFile(FileInfo);
        }

        ProviderInfo ^ provider = nullptr;

        try 
        {
            auto resolvedPaths = GetResolvedProviderPathFromPSPath(Path, provider);

            for each (String ^ path in resolvedPaths)
            {
                if (File::Exists(path))
                {
                    auto info = gcnew System::IO::FileInfo(path);
                    TouchFile(info);
                }
                else
                {
                    HandleFileNotFound(path, nullptr);
                    return;
                }
            }

        }
        catch (ItemNotFoundException ^ nf)
        {
            HandleFileNotFound(Path, nf);
        }
    }

    void SetFileTouchTimeCommand::HandleFileNotFound(String ^ path, Exception ^ exception)
    {
        auto message = String::Format(
            System::Globalization::CultureInfo::CurrentCulture,
            rm->GetString("FileNotFound"), path);
        auto ae = gcnew ArgumentException(message, exception);
        auto error = gcnew ErrorRecord(ae, "FileNotFound", ErrorCategory::ObjectNotFound, path);
        WriteError(error);
    }

    void SetFileTouchTimeCommand::TouchFile(System::IO::FileInfo ^ fileInfo)
    {
        if(ShouldProcess(fileInfo->FullName, String::Format(
                    System::Globalization::CultureInfo::CurrentCulture,
                    rm->GetString("ConfirmString"), Date)))
        {
            try
            {
                fileInfo->LastWriteTime = Date;
            }
            catch (UnauthorizedAccessException ^ uae)
            {
                auto error = gcnew ErrorRecord(uae, "UnauthorizedFileAccess", ErrorCategory::PermissionDenied, fileInfo ->FullName);
                auto detail = String::Format(
                    System::Globalization::CultureInfo::CurrentCulture,
                    rm->GetString("AccessDenied"),
                    fileInfo->FullName);
                error->ErrorDetails = gcnew ErrorDetails(detail);
                WriteError(error);
                return;
            }

            WriteObject(fileInfo);
        }
    }
}}