using namespace System;
using namespace System::Management::Automation;
using namespace System::ComponentModel;

namespace PSBook { namespace Commands
{
    [RunInstaller(true)]
    public ref class PSBookChapter2MySnapIn : PSSnapIn
    {
	public:
        // Name for the PowerShell snap-in.
        virtual property String ^ Name
        {
            String ^ get() override
            {
                return "...";
            }
        }

        // Vendor information for the PowerShell snap-in.
        virtual property String ^ Vendor
        {
            String ^ get() override
            {
                return "...";
            }
        }

        // Description of the PowerShell snap-in
        virtual property String ^ Description
        {
            String ^ get() override
            {
                return "This is a sample PowerShell snap-in";
            }
        }
    };

    // Code to implement cmdlet Write-Hi
    [Cmdlet(VerbsCommunications::Write, "HI")]
    [System::Diagnostics::CodeAnalysis::SuppressMessage("Microsoft.PowerShell", "PS1101:AllCmdletsShouldAcceptPipelineInput", Justification = "No valid input")]
    public ref class WriteHICommand : Cmdlet
    {
	protected:
		virtual void ProcessRecord() override
        {
            WriteObject("Hi, World!");
        }
    };

    // Code to implement cmdlet Write-Hello
    [Cmdlet(VerbsCommunications::Write, "Hello")]
    [System::Diagnostics::CodeAnalysis::SuppressMessage("Microsoft.PowerShell", "PS1101:AllCmdletsShouldAcceptPipelineInput", Justification = "No valid input")]
    public ref class WriteHelloCommand : Cmdlet
    {
	protected:
		virtual void ProcessRecord() override
        {
            WriteObject("Hello, World!");
        }
    };
}}

