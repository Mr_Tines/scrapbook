#pragma once
using namespace System;
using namespace System::IO;
using namespace System::Management::Automation;

namespace PSBook { namespace Commands
{
    [Cmdlet(VerbsCommon::Set, "FileTouchTime", DefaultParameterSetName = "Path", SupportsShouldProcess = true, ConfirmImpact = ConfirmImpact::Medium)]
    public ref class SetFileTouchTimeCommand :
    public PSCmdlet
    {
    public:
        SetFileTouchTimeCommand(void);

        [Parameter(ParameterSetName = "Path", Mandatory = true, Position = 1,
            ValueFromPipeline = true, ValueFromPipelineByPropertyName = true)]
        property String ^ Path;

        [Parameter(ParameterSetName = "FileInfo", Mandatory = true, Position = 1,
            ValueFromPipeline = true)]
        property FileInfo ^ FileInfo;

        [Parameter]
        property DateTime Date;

    protected:
        virtual void ProcessRecord(void) override;

    private:
        void TouchFile(System::IO::FileInfo ^ fileInfo);
        void HandleFileNotFound(String ^ path, Exception ^ exception);
        Resources::ResourceManager ^ rm;
    };
}}

