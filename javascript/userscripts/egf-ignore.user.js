/*jslint browser: true, continue: true, plusplus: true, indent: 2 */
/*global console */
// ==UserScript== 
// @name                EGF ignore user
// @namespace           https://bitbucket.org/Mr_Tines/scrapbook/src/tip/javascript/userscripts/
// @description         Hides all posts by a set of users
// @include             http://forum.evageeks.org/*
// @version             1.14.4.9
// @copyright           2013-14 Mr.Tines
// @license             WTFPL
// @grant               none
// @downloadURL         https://bitbucket.org/Mr_Tines/scrapbook/src/tip/javascript/userscripts/egf-ignore.user.js
// @updateURL           https://bitbucket.org/Mr_Tines/scrapbook/src/tip/javascript/userscripts/egf-ignore.user.js
// ==/UserScript==

(function() {
  "use strict";

  var name, text, links, l,
    // remove the ", //" to enable feature
    hidePosts, // = true,
    hidePMs, // = true,
    hidePresence, // = true,
    i = 0,
    post = document.getElementById("postrow_" + i),
    hideUsers = []; // "User1", "User2"

  while (post && hidePosts) {
    name = post.getElementsByClassName('name');
    console.log(name[0].innerHTML)
    text = name[0].getElementsByTagName("a");
    name = text[1].innerHTML;
    if (hideUsers.indexOf(name) >= 0) {
      console.log("hiding post by " + name);
      post.style.display = "none";
      text = document.getElementById("postrow_" + i + "_second");
      text.style.display = "none";      
      text = document.getElementById("postrow_" + i + "_third");
      text.style.display = "none";      
    }

    links = post.getElementsByClassName('postlink');
    for (l = 0; l < links.length; ++l) {
      text = links[l].outerHTML.length;
      name = links[l].parentNode;
      if (name.tagName !== "B") { continue; }
      text = name.innerHTML.substring(text + 1, name.innerHTML.length - 7);
      if (hideUsers.indexOf(text) >= 0) {
        name.style.border = "1px dotted black";
        while (name.tagName !== "TABLE") {
          name = name.parentNode;
        }
        // name = name.getElementsByClassName('quote')[0];
        console.log("hiding quoted text from " + text);
        name.style.display = "none";
      }
    }

    i += 1;
    post = document.getElementById("postrow_" + i);
  }
  console.log("posts and quotes hidden");

  post = document.getElementsByTagName("form");
  l = 0
  if (post) {
    l = post.length;
  }
  for (i = 0; i < l && hidePMs; ++i) {
    console.log("pm " + i);
    if (post[i] && post[i].action.indexOf("privmsg.php?folder=inbox") > 0) {
      links = post[i].getElementsByClassName('name');
      for (l = 1; l < links.length; l += 2) {
        name = links[l];
        if (hideUsers.indexOf(name.innerHTML) >= 0) {
          console.log("hiding PM from " + name.innerHTML);
          while (name.tagName !== 'TR') {
            name = name.parentNode;
          }
          text = name.getElementsByTagName("input");
          text[0].checked = true;
          name.style.display = 'none';
        }
      }
    }
  }
  console.log("pms hidden");

  post = document.getElementsByClassName("catHead")[0];
  l = 0;
  if (post) {
    post = post.parentNode.nextSibling.nextSibling.nextSibling.nextSibling;
    links = post.getElementsByTagName("A");
    l = links.length;
  }

  for (i = 0; i < l && hidePresence; ++i) {
    name = links[i].childNodes[0];
    while (name && name.nodeName !== '#text') {
      name = name.childNodes[0];
    }
    console.log(name.innerHTML);
    if (hideUsers.indexOf(name.nodeValue) >= 0) {
      console.log("hiding browsing presence of " + name.nodeValue);
      links[i].style.display = "none";
      name = links[i].nextSibling;
      if (name) {
        name.nodeValue = "";
      }
    }
  }

  l = 0;
  post = document.getElementsByClassName("maintitle")[0];
  if (post) {
    post = post.parentNode.getElementsByClassName("gensmall")[0];
  }
  if (post) {
    links = post.getElementsByTagName("A");
    l = links.length;
  }

  for (i = 0; i < l && hidePresence; ++i) {
    name = links[i].childNodes[0];
    while (name && name.nodeName !== '#text') {
      name = name.childNodes[0];
    }
    if (hideUsers.indexOf(name.nodeValue) >= 0) {
      console.log("hiding presence of " + name.innerHTML);
      links[i].style.display = "none";
      name = links[i].nextSibling;
      if (name) {
        name.nodeValue = "";
      }
    }
  }
}());