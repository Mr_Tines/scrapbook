// ==UserScript== 
// @name                EGF YouTube URL replacer
// @namespace           tines@ravnaandtines.com
// @description         Turns plaintext youtube/youtu.be URIs into VLC embedded code (not in .sigs)
// @include             http://forum.evageeks.org/*
// @version             1.13.7.27
// @copyright           2012 Mr.Tines derived from work by Scott Steiner <ScottSteiner@irc.rizon.net>, 2010
// @license             GPL version 3 or any later version; http://www.gnu.org/copyleft/gpl.html
// @grant       none
// ==/UserScript==

//reMain  = /(?:http.{3}|)(?:www.|)youtube.com\/watch\?v=([a-zA-Z0-9_-]{11})(?:&amp;(?:feature=(?:channel|feedrec|player_embedded|playlist|sub|)|index=[0-9]|p=[a-zA-Z0-9_-]{16}|playnext=[0-9]|playnext_from=TL|videos=[a-zA-Z0-9_-]{11})|){1,10}(?:<br>|)/
//reMain2 = /(?:http.{3}|)(?:www.|)youtu.be\/watch\?v=([a-zA-Z0-9_-]{11})(?:&amp;(?:feature=(?:channel|feedrec|player_embedded|playlist|sub|)|index=[0-9]|p=[a-zA-Z0-9_-]{16}|playnext=[0-9]|playnext_from=TL|videos=[a-zA-Z0-9_-]{11})|){1,10}(?:<br>|)/
//reMain3 = /(?:http.{3}|)(?:www.|)youtu.be\/([a-zA-Z0-9_-]{11})(?:&amp;(?:feature=(?:channel|feedrec|player_embedded|playlist|sub|)|index=[0-9]|p=[a-zA-Z0-9_-]{16}|playnext=[0-9]|playnext_from=TL|videos=[a-zA-Z0-9_-]{11})|){1,10}(?:<br>|)/
//reMain4 = /(?:http.{3}|)(?:www.|)vimeo.com\/([a-zA-Z0-9_-]{8})/

//embedcode = '<object width="315" height="190"><param value="http://www.youtube.com/v/$1&ap=%2526fmt%3D22" name="movie" /><param value="window" name="wmode" /><param value="true" name="allowFullScreen" /><embed width="315" height="190" wmode="window" allowfullscreen="true" type="application/x-shockwave-flash" src="http://www.youtube.com/v/$1&ap=%2526fmt%3D22"></embed></object><br />'
//embedcode2 = '<iframe src="http://player.vimeo.com/video/$1" width="320" height="240" frameborder="0"></iframe>'

reMain  = /<a href=\"(?:https?.{3}|)(?:www.|)youtube.com\/watch\?v=([a-zA-Z0-9_-]{11})[^>]*\>/
reMain2  = /<a href=\"(?:http.{3}|)(?:www.|)youtu.be\/([a-zA-Z0-9_-]{11})[^>]*\>/

embedcode = '<table><tr><td  valign="bottom"><img src="https://i2.ytimg.com/vi/$1/hqdefault.jpg" width="320" height="240" /></td>' +
'<td  valign="top"><embed src="http://www.youtube.com/watch?v=$1" type="application/x-vlc-plugin" width="320" height="240" progid="VideoLAN.VLCPlugin.2" loop="False" align="middle" showdisplay="True" autoplay="False" name="$1" id="vlc"></embed></td></tr>' + 
'<tr><td colspan="2"><center>youtu.be/$1</center></td></tr></table>';

// detect flash
var type = 'application/x-shockwave-flash';
var mimeTypes = navigator.mimeTypes;
if(mimeTypes && mimeTypes[type] && mimeTypes[type].enabledPlugin && mimeTypes[type].enabledPlugin.description) {
	embedcode = '<table><tr><td  valign="bottom"><object width="315" height="190"><param value="http://www.youtube.com/v/$1&ap=%2526fmt%3D22" name="movie" /><param value="window" name="wmode" /><param value="true" name="allowFullScreen" /><embed width="315" height="190" wmode="window" allowfullscreen="true" type="application/x-shockwave-flash" src="http://www.youtube.com/v/$1&ap=%2526fmt%3D22"></embed></object></td></tr>' +
	'<tr><td><center>youtu.be/$1</center></td></tr></table>';
}

posts = document.getElementsByClassName("postbody");
for (i = 0; i < posts.length; i++) { 
  var changed = true;
  var post = posts[i].getElementsByTagName("article")[0];
  while (changed && post) { 
    var begin = post.innerHTML.length;
    post.innerHTML = post.innerHTML.replace("feature=player_detailpage&amp;", "");
    post.innerHTML = post.innerHTML.replace("feature=player_embedded&amp;", "");
    post.innerHTML = post.innerHTML.replace(reMain, embedcode); 
    post.innerHTML = post.innerHTML.replace(reMain2, embedcode); 
    //post.innerHTML = post.innerHTML.replace(reMain3, embedcode); 
    //post.innerHTML = post.innerHTML.replace(reMain4, embedcode2); 
    changed = begin != post.innerHTML.length;
  }
}