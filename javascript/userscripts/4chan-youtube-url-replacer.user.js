// ==UserScript==
// @name		4chan YouTube URL replacer
// @namespace	        tines@ravnaandtines.com
// @description		Turns plaintext youtube URIs into embedded code
// @include		http://boards.4chan.org/*
// @include		https://boards.4chan.org/*
// @include		http://archive.foolz.us/*
// @include		http://4chanarchive.org/brchive/*
// @version		12.12.08
// @copyright		2012 Mr.Tines derived from work by Scott Steiner <ScottSteiner@irc.rizon.net>, 2010
// @license		GPL version 3 or any later version; http://www.gnu.org/copyleft/gpl.html
// ==/UserScript==

//re420	= /<a href="(?:http.{3}|)(?:www.|)youtube.com\/watch\?v=.*?" rel="nofollow">.*?\?v=([a-zA-Z0-9_-]{11}).*?<\/a>/
//reDis	= /<a rel.*>.*?\?v=([a-zA-Z0-9_-]{11}).*?<\/a>(?:<br>|)/
reMain	= /[^"\/\.](?:http.{3}|)(?:www.|)youtube.com\/watch\?v=([a-zA-Z0-9_-]{11})(?:&amp;(?:feature=(?:channel|feedrec|player_embedded|playlist|sub|)|index=[0-9]|p=[a-zA-Z0-9_-]{16}|playnext=[0-9]|playnext_from=TL|videos=[a-zA-Z0-9_-]{11})|){1,10}(?:<br>|)/
reMainA	= /[^"\/\.](?:https.{3}|)(?:www.|)youtube.com\/watch\?v=([a-zA-Z0-9_-]{11})(?:&amp;(?:feature=(?:channel|feedrec|player_embedded|playlist|sub|)|index=[0-9]|p=[a-zA-Z0-9_-]{16}|playnext=[0-9]|playnext_from=TL|videos=[a-zA-Z0-9_-]{11})|){1,10}(?:<br>|)/
reMain2	= /[^"\/\.](?:http.{3}|)(?:www.|)youtu.be\/([a-zA-Z0-9_-]{11})(?:&amp;(?:feature=(?:channel|feedrec|player_embedded|playlist|sub|)|index=[0-9]|p=[a-zA-Z0-9_-]{16}|playnext=[0-9]|playnext_from=TL|videos=[a-zA-Z0-9_-]{11})|){1,10}(?:<br>|)/

embedcode = '><table><tr><td  valign="bottom"><img src="https://i2.ytimg.com/vi/$1/hqdefault.jpg" width="320" height="240" /></td>' +
'<td  valign="top"><embed src="http://www.youtube.com/watch?v=$1" type="application/x-vlc-plugin" width="320" height="240" progid="VideoLAN.VLCPlugin.2" loop="False" align="middle" showdisplay="True" autoplay="False" name="$1" id="vlc"></embed></td></tr>' + 
'<tr><td colspan="2"><center>youtu:be code $1</center></td></tr></table>';

// detect flash
var type = 'application/x-shockwave-flash';
var mimeTypes = navigator.mimeTypes;
if(mimeTypes && mimeTypes[type] && mimeTypes[type].enabledPlugin && mimeTypes[type].enabledPlugin.description) {
	embedcode = '><table><tr><td  valign="bottom"><object width="315" height="190"><param value="http://www.youtube.com/v/$1&ap=%2526fmt%3D22" name="movie" /><param value="window" name="wmode" /><param value="true" name="allowFullScreen" /><embed width="315" height="190" wmode="window" allowfullscreen="true" type="application/x-shockwave-flash" src="http://www.youtube.com/v/$1&ap=%2526fmt%3D22"></embed></object></td></tr>' +
	'<tr><td><center><a href="http://youtu.be/$1">http://youtu.be/$1</a></center></td></tr></table>';
}

var posts = document.getElementsByTagName("blockquote");
var flag = 0;
var patterns = [ reMain, reMainA, reMain2 ];

for (i = 0; i < posts.length; i++) {
        var len = 0;
        while (len != posts[i].outerHTML.length) 
	{
                len = posts[i].outerHTML.length;
                posts[i].outerHTML = posts[i].outerHTML.replace('feature=player_embedded&amp;', '');
	}

        len = 0;
        var urls = []; 
        while (len != posts[i].outerHTML.length) 
	{
		len = posts[i].outerHTML.length;
		for (j =0; j < patterns.length; j++)
		{
			var p = patterns[j];
			match = posts[i].outerHTML.match(p);
			if (match && match.length > 1)  
			{
				var tag = match[1];
				var index = urls.indexOf(tag);
				if (index < 0)
				{ 
					urls.push(tag);
					posts[i].outerHTML = posts[i].outerHTML.replace(p, embedcode); 
				}
				else
				{
					break;
				}
			}
		}
	}
}