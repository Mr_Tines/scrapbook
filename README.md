### What are we trying to do here?

Store small pieces of code supporting blog posts in a form where they can be updated.

### What license applies to this code

Each separate Visual Studio project, each separate Java source tree, and each separate script (Python, F#, Powershell, Javascript) is a distinct entity for these purposes.

Unless specifically contradicted in a file in any such distinct entity (primarily the various Greasemonkey user scripts, some of the obsolete Java code, and the occasional stand-alone script) all code is released under the [WTFPL](http://www.wtfpl.net/).
