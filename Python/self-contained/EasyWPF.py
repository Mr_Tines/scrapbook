"""
@version: 0.3.93.1(2014-Oct-27)
@note:
EasyGui provides an easy-to-use interface for simple GUI interaction
with a user.  It does not require the programmer to know anything about
tkinter, frames, widgets, callbacks or lambda.  All GUI interactions are
invoked by simple function calls that return results.

EasyWPF is an API-compatible implementation for IronPython written against 
the Windows Presentation Foundation

Note that EasyWPF requires .net release 3.0 or greater.

EasyGui (http://easygui.sourceforge.net)
is licensed under the Creative Commons Attribution 2.0 License
http://creativecommons.org/licenses/by/2.0/

You are free to copy, distribute, and display the work, and to make derivative 
works (including translations). If you do, you must give the original author 
(http://easygui.sourceforge.net) credit. The author specifically permits (and 
encourages) teachers to post, reproduce, and distribute some or all of this 
material for use in their classes or by their students.

EasyWPF is licensed under the same terms.
"""
egversion = __doc__.split()[1]

__all__ = ['ynbox'
    , 'ccbox'
    , 'boolbox'
    , 'indexbox'
    , 'msgbox'
    , 'buttonbox'
    , 'integerbox'
    , 'multenterbox'
    , 'enterbox'
    , 'exceptionbox'
    , 'choicebox'
    , 'codebox'
    , 'textbox'
    , 'diropenbox'
    , 'fileopenbox'
    , 'filesavebox'
    , 'passwordbox'
    , 'multpasswordbox'
    , 'multchoicebox'
    , 'abouteasywpf'
    , 'egversion'
    , 'egdemo'
    , 'EgStore'
    ]

import sys
#-------------------------------------------------------------------
# Console I/O helpers
#-------------------------------------------------------------------

def write(*args):
    args = [str(arg) for arg in args]
    args = " ".join(args)
    sys.stdout.write(args)
    
def writeln(*args):
    write(*args)
    sys.stdout.write("\n")
    
def dq(s):
    return '"%s"' % s

#--------------------------------------------------
# check python version and take appropriate action
#--------------------------------------------------

if not ('subversion' in dir(sys)) or not (sys.subversion[0] == 'IronPython'):
    stars = "*"*75  
    writeln("""\n\n\n""" + stars + """
You are running Python version: 
\t\t"""+sys.version+"""
You must be using IronPython 2.6 or later to use EasyWPF.
Terminating.
""" + stars + """\n\n\n""")
    sys.exit(0)  

import os, pickle,traceback

import  clr
clr.AddReference('PresentationFramework')
clr.AddReference('PresentationCore')
clr.AddReference('System.Xml')
clr.AddReference('System.Windows.Forms')
from System import String, StringComparison, Console, Environment, Uri, Char
from System.Xml import XmlTextReader
from System.IO import StringReader, StringWriter, FileInfo, Path, BinaryReader, File
import System.Windows
from System.Windows import Thickness
from System.Windows.Markup import XamlReader
import System.Windows.Forms
import System.Windows.Controls
from Microsoft.Win32 import OpenFileDialog, SaveFileDialog
from System.Windows.Media import FontFamily

Vista = Environment.OSVersion.Version.Major >= 6

#-------------------------------------------------------------------
# Vista glass support
#-------------------------------------------------------------------

if Vista:
  from ctypes import *
  from System.Windows.Interop import WindowInteropHelper, HwndSource
  from System import IntPtr
  from System.Windows.Media import Brushes, Colors, Brush
  dwmapi = windll.dwmapi
  class __MARGINS(Structure):
    _fields_ = [("Left", c_int), ("Right", c_int), ("Top", c_int), ("Bottom", c_int)]
    
  def ExtendGlassFrame(window, margin):
    val = c_int()
    result = dwmapi.DwmIsCompositionEnabled(byref(val))
    success = result == 0 and not (val == 0)
    if not success:
      return False
      
    hwnd = WindowInteropHelper(window).Handle
    if (hwnd == IntPtr.Zero):
      return False
        
    window.Background = Brushes.Transparent
    HwndSource.FromHwnd(hwnd).CompositionTarget.BackgroundColor = Colors.Transparent
    margins = __MARGINS(int(margin.Left), 
          int(margin.Right), 
          int(margin.Top), 
          int(margin.Bottom))

    result = dwmapi.DwmExtendFrameIntoClientArea(hwnd, byref(margins))
    return result == 0
    
  class __GlassHelper:
    def __init__(self, w, t):
      self.window = w
      self.thickness = t
    
    def ApplyGlass(self):
      val = ExtendGlassFrame(self.window, self.thickness)
      hwnd = WindowInteropHelper(self.window).Handle
      HwndSource.FromHwnd(hwnd).AddHook(self.__wndproc)
    
    def __wndproc(self, hwnd, msg, wparam, lparam, handled):
      if msg == 0x031E: ## composition changed
        handled.Value = True ## set ref argument
        ExtendGlassFrame(self.window, self.thickness)
      return IntPtr.Zero
else:
  from System.Windows import SystemColors
    
#-------------------------------------------------------------------
# Xaml support
#-------------------------------------------------------------------

def LoadXaml(xaml):
    sr = StringReader(xaml)
    xr = XmlTextReader(sr)
    return XamlReader.Load(xr)
    
from System.IO.Compression import GZipStream, CompressionMode
from System.IO import MemoryStream
from System.Windows.Media.Imaging import BitmapImage, BitmapFrame

#-------------------------------------------------------------------
# unpacking my icon from the string
#-------------------------------------------------------------------

__icon64 = ('H4sIAAAAAAAEAO29B2AcSZYlJi9tynt/SvVK1+B0oQiAYBMk2JBAEOzBiM3mkuwdaUcjKasqgcplVmV'+
'dZhZAzO2dvPfee++999577733ujudTif33/8/XGZkAWz2zkrayZ4hgKrIHz9+fB8/In6NX+PX/DV+rV/'+
'jN/vNfrNf49f4NX6dX2Pr1/w1fo1fSL/xn7/Gb/drzH/tX+PXeEGfbeEz+n/6a6D9r4Mvf40/6Ndwz2+'+
'm/5fnD5Iv/6A/SH7gP3xE//xD/9A/RJ/83/y/X+P//r/lB/7DR/i78wi831R+oR//E/9NP39T+YB//k+'+
'/xm/6P/2m/xMA0b//EwPiFvLz/8Z7/ze+05//94bn1/jAZ+vX8On028mHv/ZA4/d+/q//6//6P//P//P'+
'GZvT8l//lf/k3/81/89/wN/wNf9af9Wf9jX/j3/gX/oV/If0in/wD/8A/8C/8C//CP/lP/pP/+D/+j//'+
'L//K/bF/57/67/+7P+/P+vD/uj/vj/ol/4p/4a//av/YP/8P/8D/jz/gz/qQ/6U/6Y/6YP4Y+/Fv+lr+'+
'Feve7+J/+p//pT/1T/9Q/5A/5Q/6UP+VP+cv/8r/8L/vL/rL/+D/+j/+Zf+afoV/+yr/yr6RX/uf/+X/'+
'22//z//w/T/3+w//wP/wP/oP/4L/2r/1rf/Ff/BdT+z/9T//T/9a/9W/9q/6qv+rv+Xv+nn/v3/v3/rf'+
'/7X+z7QnUv/qv/qv0yy/7Zb+MkL++vv7P//P//Jf8kl/yl/6lf+l//V//1//Vf/Vf/Uv/0r/0b//b/7Z'+
'gRf/+U//UP/Xv//v/vrz7B//BfzDBpzaE/F/xV/wV8uEv/+W//G/6m/4mIgK9Tu2JJoS59EgjpSHQh3/'+
'H3/F3/NP/9D9tcaCWf9ff9XcRYvT7v/vv/rvU46/8lb9ScCPq0S/0yb/z7/w7tv3QQyT9V/6Vf4V++Tf'+
'+jX+D8Pwf/of/YXN7mhciEf3yH/6H/+Ef+Uf+kX/1X/1Xb2hMiP11f91fRzS0n/yv/+v/Otz81/hf/pf'+
'/hUbUmZ0f8gNJ+xchbSQbf9Cvr/+n3/8k0jW/3c6v8Wv8Bge/xq/xR/8ev8av8at+F/n///wf0v//b3k'+
'PUvr/APisn8C2BAAA')
from System import Convert

__icoFile = Path.ChangeExtension(Path.GetTempFileName(), '.ico')
__iconzip = Convert.FromBase64String(__icon64)

def __GetIcon():
  mem = MemoryStream(__iconzip)
  unzip = GZipStream(mem, CompressionMode.Decompress, False)
  image = BitmapImage()
  image.BeginInit()
  image.StreamSource = unzip
  image.EndInit()
  return image

def __WriteIcon():
  mem = MemoryStream(__iconzip)
  unzip = GZipStream(mem, CompressionMode.Decompress, False)
  reader = BinaryReader(unzip)
  data = reader.ReadBytes(1206)
  reader.Close()
  unzip.Close()
  File.WriteAllBytes(__icoFile, data)

__WriteIcon()

#-------------------------------------------------------------------
# Making minimal hand-drawn title bar
#-------------------------------------------------------------------

def __FurnishWindow(title):
    boxRoot.Title = title
    boxRoot.FindName('title').Content = title
    boxRoot.FindName('icon').Source = __GetIcon()
    
    uri = Uri(__icoFile)
    boxRoot.Icon = BitmapFrame.Create(uri)
    
    boxRoot.MouseLeftButtonDown  += lambda s,e : boxRoot.DragMove()
      
    if Vista:
      boxRoot.AllowsTransparency = False
      boxRoot.Background = Brushes.Transparent
      boxRoot.FindName('title').Background = Brushes.Transparent
      thickness = Thickness(0.0, float(boxRoot.FindName('title').Height), 0.0, 0.0)
      helper = __GlassHelper(boxRoot, thickness)
      boxRoot.SourceInitialized += lambda s,e : helper.ApplyGlass()
    else:
      def activeColor():
        boxRoot.FindName('title').Background = SystemColors.ActiveBorderBrush  
        boxRoot.FindName('banner').Background = SystemColors.ActiveBorderBrush  
        boxRoot.FindName('title').Foreground = SystemColors.ActiveCaptionTextBrush
      def inactiveColor():
        boxRoot.FindName('title').Background = SystemColors.InactiveBorderBrush  
        boxRoot.FindName('banner').Background = SystemColors.InactiveBorderBrush  
        boxRoot.FindName('title').Foreground = SystemColors.InactiveCaptionTextBrush
      
      boxRoot.Activated += lambda s,e: activeColor()
      boxRoot.Deactivated += lambda s,e: inactiveColor()
      activeColor()    

#-------------------------------------------------------------------
# repeated code in EasyGUI -- for compatibility, still limit to GIF support
#-------------------------------------------------------------------

def __ValidateImage(image, msg):
    if image:
        image = os.path.normpath(image)
        junk,ext = os.path.splitext(image)
        if ext.lower() == ".gif":
            if os.path.exists(image):
                pass
            else:
                msg += ImageErrorMsg % (image, "Image file not found.")
                image = None
        else:
            msg += ImageErrorMsg % (image, "Image file is not a .gif file.")
            image = None
    return (image, msg)


# Initialize some global variables that will be reset later
__choiceboxMultipleSelect = None
__widgetTexts = None
__replyButtonText = None
__choiceboxResults = None
__firstWidget = None
__enterboxText = None
__enterboxDefaultText=""
__multenterboxText = ""
choiceboxChoices = None
choiceboxWidget = None
entryWidget = None
boxRoot = None
ImageErrorMsg = (
    "\n\n---------------------------------------------\n"
    "Error: %s\n%s")
root = None    
#-------------------------------------------------------------------
# various boxes built on top of the basic buttonbox
#-----------------------------------------------------------------------

#-----------------------------------------------------------------------
# ynbox
#-----------------------------------------------------------------------
def ynbox(msg="Shall I continue?"
    , title=" "
    , choices=("Yes", "No")
    , image=None
    ):
    """
    Display a msgbox with choices of Yes and No.

    The default is "Yes".

    The returned value is calculated this way::
        if the first choice ("Yes") is chosen, or if the dialog is cancelled:
            return 1
        else:
            return 0

    If invoked without a msg argument, displays a generic request for a confirmation
    that the user wishes to continue.  So it can be used this way::
        if ynbox(): pass # continue
        else: sys.exit(0)  # exit the program

    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg choices: a list or tuple of the choices to be displayed
    """
    return boolbox(msg, title, choices, image=image)


#-----------------------------------------------------------------------
# ccbox
#-----------------------------------------------------------------------
def ccbox(msg="Shall I continue?"
    , title=" "
    , choices=("Continue", "Cancel")
    , image=None
    ):
    """
    Display a msgbox with choices of Continue and Cancel.

    The default is "Continue".

    The returned value is calculated this way::
        if the first choice ("Continue") is chosen, or if the dialog is cancelled:
            return 1
        else:
            return 0

    If invoked without a msg argument, displays a generic request for a confirmation
    that the user wishes to continue.  So it can be used this way::

        if ccbox():
            pass # continue
        else:
            sys.exit(0)  # exit the program

    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg choices: a list or tuple of the choices to be displayed
    """
    return boolbox(msg, title, choices, image=image)


#-----------------------------------------------------------------------
# boolbox
#-----------------------------------------------------------------------
def boolbox(msg="Shall I continue?"
    , title=" "
    , choices=("Yes","No")
    , image=None
    ):
    """
    Display a boolean msgbox.

    The default is the first choice.

    The returned value is calculated this way::
        if the first choice is chosen, or if the dialog is cancelled:
            returns 1
        else:
            returns 0
    """
    reply = buttonbox(msg=msg, choices=choices, title=title, image=image)
    if reply == choices[0]: return 1
    else: return 0


#-----------------------------------------------------------------------
# indexbox
#-----------------------------------------------------------------------
def indexbox(msg="Shall I continue?"
    , title=" "
    , choices=("Yes","No")
    , image=None
    ):
    """
    Display a buttonbox with the specified choices.
    Return the index of the choice selected.
    """
    reply = buttonbox(msg=msg, choices=choices, title=title, image=image)
    index = -1
    for choice in choices:
        index = index + 1
        if reply == choice: return index
    raise AssertionError(
        "There is a program logic error in the EasyGui code for indexbox.")


#-----------------------------------------------------------------------
# msgbox
#-----------------------------------------------------------------------
def msgbox(msg="(Your message goes here)", title=" ", ok_button="OK",image=None,root=None):
    """
    Display a messagebox
    """
    if type(ok_button) != type("OK"):
        raise AssertionError("The 'ok_button' argument to msgbox must be a string.")

    return buttonbox(msg=msg, title=title, choices=[ok_button], image=image,root=root)

#-------------------------------------------------------------------
# buttonbox
#-------------------------------------------------------------------
def buttonbox(msg="",title=" "
    ,choices=("Button1", "Button2", "Button3")
    , image=None
    , root=None
    ):
    """
    Display a msg, a title, and a set of buttons.
    The buttons are defined by the members of the choices list.
    Return the text of the button that the user selected.

    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg choices: a list or tuple of the choices to be displayed
    """
    global boxRoot, __replyButtonText, __widgetTexts, buttonsFrame
    
    # Initialize __replyButtonText to the first choice.
    # This is what will be used if the window is closed by the close button.
    __replyButtonText = choices[0]
    
    xaml = '''<Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="Window2" SizeToContent="WidthAndHeight" MinWidth="400" MinHeight="150"  WindowStyle="None" AllowsTransparency="True">
    <Grid>
        <Grid Name="banner">
            <Image Name="icon" Stretch="Fill" HorizontalAlignment="Left" VerticalAlignment="Top" Width="20" Height="20" Margin="4,4,0,0" />
            <Label Height="28" Name="title" VerticalAlignment="Top" Background="WhiteSmoke" Margin="28,0,0,0">Label</Label>
        </Grid>
        <DockPanel Margin="0,28,0,0" Name="backdrop">
          <StackPanel Name="buttons" DockPanel.Dock="Bottom" HorizontalAlignment="Center" Orientation="Horizontal">
            <!-- Button Height="23" Name="button1" Width="75" Margin="2">Button</Button -->
          </StackPanel>

          <StackPanel  Name="body" VerticalAlignment="Top">
            <TextBlock Name="message" VerticalAlignment="Top" Margin="5" />
            <Image Name="image" Stretch="None" />
          </StackPanel>
        </DockPanel>
    </Grid>
</Window>'''
    boxRoot = LoadXaml(xaml)
    if root:
      boxRoot.Owner = root
      boxRoot.Top = root.Top
      boxRoot.Left = root.Left
      root.Visibility = System.Windows.Visibility.Hidden
    
    __FurnishWindow(title)
    
    if Vista:
      brush = Brushes.White
    else:
      brush = SystemColors.WindowBrush
    
    for name in ('body', 'buttons', 'backdrop'):
       boxRoot.FindName(name).Background = brush

    (image, msg) = __ValidateImage(image, msg)
    boxRoot.FindName('message').Text = msg

    if image:
      bi = BitmapImage()
      bi.BeginInit()
      image = FileInfo(image).FullName
      bi.UriSource = Uri(image)
      bi.EndInit()
      
      pane = boxRoot.FindName('image')
      pane.BeginInit()
      pane.Source = bi
      pane.EndInit()
    else:
      boxRoot.FindName('image').Visibility = System.Windows.Visibility.Collapsed
      
    if len(choices) < 1:
      choices = ('Ok')
      
    first = None
    for choice in choices:
      x = System.Windows.Controls.Button()
      x.Content = choice
      x.Click += __buttonEvent
      x.MinHeight=23 
      x.MinWidth=75 
      x.Margin= Thickness(2)
      x.HorizontalAlignment = System.Windows.HorizontalAlignment.Center
      boxRoot.FindName('buttons').Children.Add(x)
      if not first:
        first = x
        
    first.Focus()
    boxRoot.ShowDialog()
    
    if root:
      root.Visibility = System.Windows.Visibility.Visible
    return __replyButtonText

#-------------------------------------------------------------------
# integerbox
#-------------------------------------------------------------------
def integerbox(msg=""
    , title=" "
    , default=""
    , argLowerBound=0
    , argUpperBound=99
    , image = None
    , root  = None
    ):
    """
    Show a box in which a user can enter an integer.

    In addition to arguments for msg and title, this function accepts
    integer arguments for default_value, lowerbound, and upperbound.

    The default_value argument may be None.

    When the user enters some text, the text is checked to verify
    that it can be converted to an integer between the lowerbound and upperbound.

    If it can be, the integer (not the text) is returned.

    If it cannot, then an error msg is displayed, and the integerbox is
    redisplayed.

    If the user cancels the operation, None is returned.
    """
    if default != "":
        if type(default) != type(1):
            raise AssertionError(
                "integerbox received a non-integer value for "
                + "default of " + dq(str(default)) , "Error")

    if type(argLowerBound) != type(1):
        raise AssertionError(
            "integerbox received a non-integer value for "
            + "argLowerBound of " + dq(str(argLowerBound)) , "Error")

    if type(argUpperBound) != type(1):
        raise AssertionError(
            "integerbox received a non-integer value for "
            + "argUpperBound of " + dq(str(argUpperBound)) , "Error")

    if msg == "":
        msg = ("Enter an integer between " + str(argLowerBound)
            + " and "
            + str(argUpperBound)
            )

    while 1:
        reply = enterbox(msg, title, str(default), image=image, root=root)
        if reply == None: return None

        try:
            reply = int(reply)
        except:
            msgbox ("The value that you entered:\n\t%s\nis not an integer." % dq(str(reply))
                    , "Error")
            continue

        if reply < argLowerBound:
            msgbox ("The value that you entered is less than the lower bound of "
                + str(argLowerBound) + ".", "Error")
            continue

        if reply > argUpperBound:
            msgbox ("The value that you entered is greater than the upper bound of "
                + str(argUpperBound) + ".", "Error")
            continue

        # reply has passed all validation checks.
        # It is an integer between the specified bounds.
        return reply

#-------------------------------------------------------------------
# multenterbox
#-------------------------------------------------------------------
def multenterbox(msg="Fill in values for the fields."
    , title=" "
    , fields=()
    , values=()
    ):
    r"""
    Show screen with multiple data entry fields.
    
    If there are fewer values than names, the list of values is padded with
    empty strings until the number of values is the same as the number of names.
    
    If there are more values than names, the list of values
    is truncated so that there are as many values as names.
    
    Returns a list of the values of the fields,
    or None if the user cancels the operation.
    
    Here is some example code, that shows how values returned from
    multenterbox can be checked for validity before they are accepted::
        ----------------------------------------------------------------------
        msg = "Enter your personal information"
        title = "Credit Card Application"
        fieldNames = ["Name","Street Address","City","State","ZipCode"]
        fieldValues = []  # we start with blanks for the values
        fieldValues = multenterbox(msg,title, fieldNames)
    
        # make sure that none of the fields was left blank
        while 1:
            if fieldValues == None: break
            errmsg = ""
            for i in range(len(fieldNames)):
                if fieldValues[i].strip() == "":
                    errmsg += ('"%s" is a required field.\n\n' % fieldNames[i])
            if errmsg == "":
                break # no problems found
            fieldValues = multenterbox(errmsg, title, fieldNames, fieldValues)
        
        writeln("Reply was: %s" % str(fieldValues))
        ----------------------------------------------------------------------
    
    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg fields: a list of fieldnames.
    @arg values:  a list of field values
    """
    return __multfillablebox(msg,title,fields,values,None)


#-----------------------------------------------------------------------
# multpasswordbox
#-----------------------------------------------------------------------
def multpasswordbox(msg="Fill in values for the fields."
    , title=" "
    , fields=tuple()
    ,values=tuple()
    ):
    r"""
    Same interface as multenterbox.  But in multpassword box,
    the last of the fields is assumed to be a password, and
    is masked with asterisks.

    Example
    =======

    Here is some example code, that shows how values returned from
    multpasswordbox can be checked for validity before they are accepted::
        msg = "Enter logon information"
        title = "Demo of multpasswordbox"
        fieldNames = ["Server ID", "User ID", "Password"]
        fieldValues = []  # we start with blanks for the values
        fieldValues = multpasswordbox(msg,title, fieldNames)

        # make sure that none of the fields was left blank
        while 1:
            if fieldValues == None: break
            errmsg = ""
            for i in range(len(fieldNames)):
                if fieldValues[i].strip() == "":
                    errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
                if errmsg == "": break # no problems found
            fieldValues = multpasswordbox(errmsg, title, fieldNames, fieldValues)
        
        writeln("Reply was: %s" % str(fieldValues))
    """
    return __multfillablebox(msg,title,fields,values,"*")

#-----------------------------------------------------------------------
# __multfillablebox
#-----------------------------------------------------------------------
def __multfillablebox(msg="Fill in values for the fields."
    , title=" "
    , fields=()
    , values=()
    , mask = None
    ):
    global boxRoot, __multenterboxText, __multenterboxDefaultText, cancelButton, entryWidget, okButton

    choices = ["OK", "Cancel"]
    if len(fields) == 0: return None

    fields = list(fields[:])  # convert possible tuples to a list
    values = list(values[:])  # convert possible tuples to a list

    if   len(values) == len(fields): pass
    elif len(values) >  len(fields):
        fields = fields[0:len(values)]
    else:
        while len(values) < len(fields):
            values.append("")
            
    xaml = '''<Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="Window2" SizeToContent="WidthAndHeight" MinWidth="400" MinHeight="150"  WindowStyle="None" AllowsTransparency="True">
    <Grid>
        <Grid Name="banner">
            <Image Name="icon" Stretch="Fill" HorizontalAlignment="Left" VerticalAlignment="Top" Width="20" Height="20" Margin="4,4,0,0" />
            <Label Height="28" Name="title" VerticalAlignment="Top" Background="WhiteSmoke" Margin="28,0,0,0">Label</Label>
        </Grid>
        <DockPanel Margin="0,28,0,0" Name="backdrop">
          <StackPanel Name="buttons" DockPanel.Dock="Bottom" HorizontalAlignment="Center" Orientation="Horizontal">
          </StackPanel>
          <TextBlock Name="message" DockPanel.Dock="Top" VerticalAlignment="Top" Margin="5" />
          <DockPanel Name="entries">
            <StackPanel Name="fields" DockPanel.Dock="Left" >
            </StackPanel>
            <StackPanel Name="values" >
            </StackPanel>            
          </DockPanel>
        </DockPanel>
    </Grid>
</Window>'''
    boxRoot = LoadXaml(xaml)
    if root:
      boxRoot.Owner = root
      boxRoot.Top = root.Top
      boxRoot.Left = root.Left
      root.Visibility = System.Windows.Visibility.Hidden
    
    __FurnishWindow(title)
    
    if Vista:
      brush = Brushes.White
    else:
      brush = SystemColors.WindowBrush
    
    #for name in ('backdrop',):
    #   boxRoot.FindName(name).Background = brush
    boxRoot.FindName('backdrop').Background = brush
    boxRoot.FindName('message').Text = msg
      
    buttons = []
    for choice in ("Ok", "Cancel"):
      x = System.Windows.Controls.Button()
      x.Content = choice
      x.Height=23 
      x.Width=75 
      x.Margin= Thickness(2)
      boxRoot.FindName('buttons').Children.Add(x)
      buttons.append(x)

    buttons[0].Click += lambda s, e: __multenterboxGetText(mask)
    buttons[0].IsDefault = True
    buttons[1].Click += lambda s, e: __multenterboxCancel()
    buttons[1].IsCancel = True

    global entryWidgets
    entryWidgets = []
    labelWidgets = []
    first = None
    
    lastWidgetIndex = len(fields) - 1
    left = boxRoot.FindName('fields')
    right = boxRoot.FindName('values')

    for widgetIndex in range(len(fields)):
        argFieldName  = fields[widgetIndex]
        argFieldValue = values[widgetIndex]
        
        label = System.Windows.Controls.Label()
        label.Content = argFieldName
        left.Children.Add(label)
        labelWidgets.append(label)
        label.Margin = Thickness(2)
        
        if mask and (widgetIndex == lastWidgetIndex):
            entry = System.Windows.Controls.PasswordBox()
            entry.PasswordChar = mask
            entry.Password = argFieldValue
        else:
            entry = System.Windows.Controls.TextBox()
            entry.Text = argFieldValue
            
        if not first:
            first = entry
            
        entry.Margin = Thickness(2)
        right.Children.Add(entry)
        entryWidgets.append(entry)
        

    # ------------------- time for action! -----------------
    first.Focus()    # put the focus on the entryWidget
    
    boxRoot.SizeChanged += lambda s,e : __multienteradjust(entryWidgets, labelWidgets)

    boxRoot.ShowDialog()
    if root:
      root.Visibility = System.Windows.Visibility.Visible
    return __multenterboxText
    
#-----------------------------------------------------------------------
# __multienteradjust -- fiddle to get the heights to match up
#-----------------------------------------------------------------------
def __multienteradjust(entryWidgets, labelWidgets):
  h = max (entryWidgets[0].ActualHeight, labelWidgets[0].ActualHeight)
  for e in entryWidgets:
    e.Height = h
    e.MinHeight = h
  for l in labelWidgets:
    l.Height = h
    l.MinHeight = h 
 
#-----------------------------------------------------------------------
# __multenterboxGetText
#-----------------------------------------------------------------------
def __multenterboxGetText(mask):
    global __multenterboxText

    __multenterboxText = []
    for entryWidget in entryWidgets[:-1]:
        __multenterboxText.append(entryWidget.Text)
        
    entry = entryWidgets[-1]
    __multenterboxText.append(entry.Password if mask else entry.Text)       
    __finish()


def __multenterboxCancel():
    global __multenterboxText
    __multenterboxText = None
    __finish()


#-------------------------------------------------------------------
# enterbox
#-------------------------------------------------------------------
def enterbox(msg="Enter something."
    , title=" "
    , default=""
    , strip=True
    , image=None
    , root=None
    ):
    """
    Show a box in which a user can enter some text.

    You may optionally specify some default text, which will appear in the
    enterbox when it is displayed.

    Returns the text that the user entered, or None if he cancels the operation.

    By default, enterbox strips its result (i.e. removes leading and trailing
    whitespace).  (If you want it not to strip, use keyword argument: strip=False.)
    This makes it easier to test the results of the call::

        reply = enterbox(....)
        if reply:
            ...
        else:
            ...
    """
    result = __fillablebox(msg, title, default=default, mask=None,image=image,root=root)
    if result and strip:
        result = result.strip()
    return result


def passwordbox(msg="Enter your password."
    , title=" "
    , default=""
    , image=None
    , root=None
    ):
    """
    Show a box in which a user can enter a password.
    The text is masked with asterisks, so the password is not displayed.
    Returns the text that the user entered, or None if he cancels the operation.
    """
    return __fillablebox(msg, title, default, mask="*",image=image,root=root)


def __fillablebox(msg
    , title=""
    , default=""
    , mask=None
    , image=None
    , root=None
    ):
    """
    Show a box in which a user can enter some text.
    You may optionally specify some default text, which will appear in the
    enterbox when it is displayed.
    Returns the text that the user entered, or None if he cancels the operation.
    """

    global boxRoot, __enterboxText, __enterboxDefaultText
    global cancelButton, entryWidget, okButton

    if title == None: title == ""
    if default == None: default = ""
    __enterboxDefaultText = default
    __enterboxText        = __enterboxDefaultText

    xaml = '''<Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="Window2" SizeToContent="WidthAndHeight" MinWidth="400" MinHeight="150"  WindowStyle="None" AllowsTransparency="True">
    <Grid>
        <Grid Name="banner">
            <Image Name="icon" Stretch="Fill" HorizontalAlignment="Left" VerticalAlignment="Top" Width="20" Height="20" Margin="4,4,0,0" />
            <Label Height="28" Name="title" VerticalAlignment="Top" Background="WhiteSmoke" Margin="28,0,0,0">Label</Label>
        </Grid>
        <DockPanel Margin="0,28,0,0" Name="backdrop">
          <StackPanel Name="buttons" DockPanel.Dock="Bottom" HorizontalAlignment="Center" Orientation="Horizontal">
            <!-- Button Height="23" Name="button1" Width="75" Margin="2">Button</Button -->
          </StackPanel>

          <StackPanel  Name="body" VerticalAlignment="Top">
            <TextBlock Name="message" VerticalAlignment="Top" Margin="5" />
            <Image Name="image" Stretch="None" />
          </StackPanel>
        </DockPanel>
    </Grid>
</Window>'''
    boxRoot = LoadXaml(xaml)
    if root:
      boxRoot.Owner = root
      boxRoot.Top = root.Top
      boxRoot.Left = root.Left
      root.Visibility = System.Windows.Visibility.Hidden
      
    __FurnishWindow(title)
    
    if Vista:
      brush = Brushes.White
    else:
      brush = SystemColors.WindowBrush
    
    for name in ('body', 'buttons', 'backdrop'):
       boxRoot.FindName(name).Background = brush

    (image, msg) = __ValidateImage(image, msg)
    boxRoot.FindName('message').Text = msg
    
    if image:
      bi = BitmapImage()
      bi.BeginInit()
      image = FileInfo(image).FullName
      bi.UriSource = Uri(image)
      bi.EndInit()
      
      pane = boxRoot.FindName('image')
      pane.BeginInit()
      pane.Source = bi
      pane.EndInit()
    else:
      boxRoot.FindName('image').Visibility = System.Windows.Visibility.Collapsed

    if mask:
      entry = System.Windows.Controls.PasswordBox()
      entry.PasswordChar = mask
      entry.Password = __enterboxDefaultText
    else:
      entry = System.Windows.Controls.TextBox()
      entry.Text = __enterboxDefaultText
      
    entry.Height = 22
    entry.Margin = Thickness(5)
    boxRoot.FindName('body').Children.Add(entry)
    entry.Focus()
    
    buttons = []
    for choice in ("Ok", "Cancel"):
      x = System.Windows.Controls.Button()
      x.Content = choice
      x.Height=23 
      x.Width=75 
      x.Margin= Thickness(2)
      boxRoot.FindName('buttons').Children.Add(x)
      buttons.append(x)

    buttons[0].Click += lambda s, e: __enterboxGetText(entry, mask)
    buttons[0].IsDefault = True
    buttons[1].Click += lambda s, e: __enterboxCancel()
    buttons[1].IsCancel = True

    boxRoot.ShowDialog()
    if root:
      root.Visibility = System.Windows.Visibility.Visible
    return __enterboxText

def __finish():
    global boxRoot
    boxRoot.Hide()
    boxRoot.Close()
    boxRoot = None  

def __enterboxGetText(entry, mask):
    global __enterboxText
    __enterboxText = entry.Password if mask else entry.Text   
    __finish()


def __enterboxCancel():
    global __enterboxText
    __enterboxText = None
    __finish()

#-------------------------------------------------------------------
# multchoicebox
#-------------------------------------------------------------------
def multchoicebox(msg="Pick as many items as you like."
    , title=" "
    , choices=()
    , **kwargs
    ):
    """
    Present the user with a list of choices.
    allow him to select multiple items and return them in a list.
    if the user doesn't choose anything from the list, return the empty list.
    return None if he cancelled selection.

    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg choices: a list or tuple of the choices to be displayed
    """
    if len(choices) == 0: choices = ["Program logic error - no choices were specified."]

    global __choiceboxMultipleSelect
    __choiceboxMultipleSelect = 1
    return __choicebox(msg, title, choices)


#-----------------------------------------------------------------------
# choicebox
#-----------------------------------------------------------------------
def choicebox(msg="Pick something."
    , title=" "
    , choices=()
    , buttons=()
    ):
    """
    Present the user with a list of choices.
    return the choice that he selects.
    return None if he cancels the selection selection.

    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg choices: a list or tuple of the choices to be displayed
    """
    if len(choices) == 0: choices = ["Program logic error - no choices were specified."]

    global __choiceboxMultipleSelect
    __choiceboxMultipleSelect = 0
    return __choicebox(msg,title,choices,buttons)


#-----------------------------------------------------------------------
# __choicebox
#-----------------------------------------------------------------------
def __choicebox(msg
    , title
    , choices
    ,  buttons=()
    ):
    """
    internal routine to support choicebox() and multchoicebox()
    """
    global boxRoot, __choiceboxResults, choiceboxWidget, defaultText
    global choiceboxChoices
    #-------------------------------------------------------------------
    # If choices is a tuple, we make it a list so we can sort it.
    # If choices is already a list, we make a new list, so that when
    # we sort the choices, we don't affect the list object that we
    # were given.
    #-------------------------------------------------------------------
    choices = list(choices[:])
    if len(choices) == 0:
        choices = ["Program logic error - no choices were specified."]
    defaultButtons = ["OK", "Cancel"]

    # make sure all choices are strings
    choices = [x.ToString() for x in choices]

    if buttons:
        if type(buttons) == type("abc"): # user sent a string
            choiceboxButtons = [buttons]
        else: # we assume user sent in a list or tuple of strings
            choiceboxButtons = [x.ToString() for x in buttons]
    else:
        choiceboxButtons = defaultButtons

    if title == None: title = ""

    # Initialize __choiceboxResults
    # This is the value that will be returned if the user clicks the close icon
    __choiceboxResults = None

    xaml = '''<Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    Title="Window1" SizeToContent="WidthAndHeight" MinWidth="400" MinHeight="150" WindowStyle="None" AllowsTransparency="True">
    <Grid>
        <Grid Name="banner">
            <Image Name="icon" Stretch="Fill" HorizontalAlignment="Left" VerticalAlignment="Top" Width="20" Height="20" Margin="4,4,0,0" />
            <Label Height="28" Name="title" VerticalAlignment="Top" Background="WhiteSmoke" Margin="28,0,0,0">Label</Label>
        </Grid>
        <DockPanel Margin="0,28,0,0" Name='backdrop'>
          <ListBox Name="listBox" DockPanel.Dock="Bottom" MaxHeight="300" />
        
          <StackPanel Name="stackPanel" VerticalAlignment="Top" HorizontalAlignment="Right" DockPanel.Dock="Right"  MinHeight="100" >
            <Button MinHeight="23" Name="ok" MinWidth="73" Margin="1">OK</Button>
            <Button MinHeight="23" Name="cancel" MinWidth="73" Margin="1">Cancel</Button>
            <Button MinHeight="23" Name="select" MinWidth="73" Margin="1">Select All</Button>
            <Button Height="23" Name="clear" MinWidth="73" Margin="1">Clear All</Button>
          </StackPanel>
          <TextBlock Name="message" VerticalAlignment="Top" Text="message"  Padding="5" TextWrapping="Wrap" MinHeight="100" />
        </DockPanel>
    </Grid>
</Window>'''
    boxRoot = LoadXaml(xaml)

    boxRoot.FindName('message').Text = msg

    if Vista:
      brush = Brushes.White
    else:
      brush = SystemColors.WindowBrush
      
    for control in ('message', 'stackPanel', 'backdrop'):
      boxRoot.FindName(control).Background = brush

    __FurnishWindow(title)
      
    #---------------------------------------------------
    # sort the choices
    # eliminate duplicates
    # put the choices into the choiceboxWidget
    #---------------------------------------------------
    choices = set(choices)
    choices = [x for x in choices]
    choices.sort(cmp = lambda s1, s2 : String.Compare(s1, s2, StringComparison.CurrentCultureIgnoreCase))
    
    listBox = boxRoot.FindName('listBox')
    listBox.ItemsSource = choices
    choiceboxWidget = listBox
    choiceboxChoices = choices

    if __choiceboxMultipleSelect:
        listBox.SelectionMode = System.Windows.Controls.SelectionMode.Extended
    
    if len(choices) == 0:
      boxRoot.FindName('ok').Visibility = System.Windows.Visibility.Hidden
      listBox.MouseDoubleClick += __choiceboxCancel
    else:
      listBox.MouseDoubleClick += __choiceboxGetChoice
      listBox.SelectedItem = choices[0]
      listBox.Focus()
      
    if len(choices) == 0 or not __choiceboxMultipleSelect:
      boxRoot.FindName('select').Visibility = System.Windows.Visibility.Hidden
      boxRoot.FindName('clear').Visibility = System.Windows.Visibility.Hidden
    else:
      boxRoot.FindName('select').Click += __choiceboxSelectAll
      boxRoot.FindName('clear').Click += __choiceboxClearAll
      
    boxRoot.FindName('cancel').Click += __choiceboxCancel
    boxRoot.FindName('cancel').IsCancel = True
    boxRoot.FindName('ok').Click += __choiceboxGetChoice
    boxRoot.FindName('ok').IsDefault = True
    
    boxRoot.KeyUp += KeyboardListener
    
    # --- run it! -----
    boxRoot.ShowDialog()
    return __choiceboxResults

def __choiceboxGetChoice(source, event):
    global boxRoot, __choiceboxResults, choiceboxWidget
    if __choiceboxMultipleSelect:
        __choiceboxResults = [x for x in choiceboxWidget.SelectedItems]
    else:
        __choiceboxResults = choiceboxWidget.SelectedItem
    __finish()

def __choiceboxSelectAll(source, event):
    global choiceboxWidget, choiceboxChoices
    choiceboxWidget.SelectAll()

def __choiceboxClearAll(source, event):
    global choiceboxWidget, choiceboxChoices
    choiceboxWidget.SelectedIndex = -1

def __choiceboxCancel(source, event):
    global boxRoot, __choiceboxResults
    __choiceboxResults = None
    __finish()


def KeyboardListener(source, event):
    global choiceboxChoices, choiceboxWidget
    key = event.Key.ToString()
    if len(key) <= 1:
        if not Char.IsControl(key):
            # Find the key in the list.
            # before we clear the list, remember the selected member
            try:
                start_n = choiceboxWidget.SelectedIndex
            except IndexError:
                start_n = -1

            ## clear the selection.
            choiceboxWidget.SelectedIndex = -1

            ## start from previous selection +1
            for n in range(start_n+1, len(choiceboxChoices)):
                item = choiceboxChoices[n]
                if item[0].lower() == key.lower():
                    choiceboxWidget.SelectedIndex = n
                    choiceboxWidget.ScrollIntoView(item)
                    return
            else:
                # has not found it so loop from top
                for n in range(len(choiceboxChoices)):
                    item = choiceboxChoices[n]
                    if item[0].lower() == key.lower():
                        choiceboxWidget.SelectedIndex = n
                        choiceboxWidget.ScrollIntoView(item)
                        return

                # nothing matched -- we'll look for the next logical choice
                for n in range(len(choiceboxChoices)):
                    item = choiceboxChoices[n]
                    if item[0].lower() > key.lower():
                        if n > 0:
                            choiceboxWidget.SelectedIndex = (n-1)
                        else:
                            choiceboxWidget.SelectedIndex = 0
                        choiceboxWidget.ScrollIntoView(item)
                        return

                # still no match (nothing was greater than the key)
                # we set the selection to the first item in the list
                lastIndex = len(choiceboxChoices)-1
                choiceboxWidget.SelectedIndex = lastIndex
                item = choiceboxChoices[lastIndex]
                choiceboxWidget.ScrollIntoView(item)
                return

#-----------------------------------------------------------------------
# exception_format
#-----------------------------------------------------------------------
def exception_format():
    """
    Convert exception info into a string suitable for display.
    """
    return "".join(traceback.format_exception(
           sys.exc_info()[0]
        ,  sys.exc_info()[1]
        ,  sys.exc_info()[2]
        ))

#-----------------------------------------------------------------------
# exceptionbox
#-----------------------------------------------------------------------
def exceptionbox(msg=None, title=None):
    """
    Display a box that gives information about
    an exception that has just been raised.

    The caller may optionally pass in a title for the window, or a
    msg to accompany the error information.

    Note that you do not need to (and cannot) pass an exception object
    as an argument.  The latest exception will automatically be used.
    """
    if title == None: title = "Error Report"
    if msg == None:
        msg = "An error (exception) has occurred in the program."

    codebox(msg, title, exception_format())

#-------------------------------------------------------------------
# codebox
#-------------------------------------------------------------------

def codebox(msg=""
    , title=" "
    , text=""
    ):
    """
    Display some text in a monospaced font, with no line wrapping.
    This function is suitable for displaying code and text that is
    formatted using spaces.

    The text parameter should be a string, or a list or tuple of lines to be
    displayed in the textbox.
    """
    textbox(msg, title, text, codebox=1 )

#-------------------------------------------------------------------
# textbox
#-------------------------------------------------------------------
def textbox(msg=""
    , title=" "
    , text=""
    , codebox=0
    ):
    """
    Display some text in a proportional font with line wrapping at word breaks.
    This function is suitable for displaying general written text.

    The text parameter should be a string, or a list or tuple of lines to be
    displayed in the textbox.
    """

    if msg == None: msg = ""
    if title == None: title = ""

    global boxRoot, __replyButtonText, __widgetTexts, buttonsFrame
    global rootWindowPosition

    xaml = '''<Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    Title="Window1" SizeToContent="WidthAndHeight" MinWidth="400" MinHeight="150" WindowStyle="None" AllowsTransparency="True">
    <Grid>
        <Grid Name="banner">
            <Image Name="icon" Stretch="Fill" HorizontalAlignment="Left" VerticalAlignment="Top" Width="20" Height="20" Margin="4,4,0,0" />
            <Label Height="28" Name="title" VerticalAlignment="Top" Background="WhiteSmoke" Margin="28,0,0,0">Label</Label>
        </Grid>
        <DockPanel Margin="0,28,0,0" Name='backdrop'>
          <ScrollViewer DockPanel.Dock="Bottom" MaxHeight="300" >
            <TextBlock Name="content" />
          </ScrollViewer>        
          <StackPanel Name="stackPanel" VerticalAlignment="Top" HorizontalAlignment="Right" DockPanel.Dock="Right"  MinHeight="100" >
            <Button MinHeight="23" Name="ok" MinWidth="73" Margin="1">OK</Button>
          </StackPanel>
          <TextBlock Name="message" VerticalAlignment="Top" Text="message"  Padding="5" TextWrapping="Wrap" MinHeight="100" />
        </DockPanel>
    </Grid>
</Window>'''
    boxRoot = LoadXaml(xaml)

    boxRoot.FindName('message').Text = msg

    __FurnishWindow(title)
    content = boxRoot.FindName('content')
    
    if Vista:
      boxRoot.FindName('message').Background = Brushes.White
      boxRoot.FindName('stackPanel').Background = Brushes.White
      boxRoot.FindName('backdrop').Background = Brushes.White
      content.Background = Brushes.White
      content.Foreground = Brushes.Black
    else:
      boxRoot.FindName('message').Background = SystemColors.WindowBrush
      boxRoot.FindName('stackPanel').Background = SystemColors.WindowBrush
      boxRoot.FindName('backdrop').Background = SystemColors.WindowBrush
      content.Background = SystemColors.WindowBrush
      content.Background = SystemColors.WindowTextBrush
      
    # put a textbox in the top frame

    if codebox:
      content.TextWrapping = System.Windows.TextWrapping.NoWrap
      content.FontFamily = FontFamily('GlobalMonospace.CompositeFont')
    else:
      content.TextWrapping = System.Windows.TextWrapping.WrapWithOverflow
      content.FontFamily = FontFamily('GlobalSanSerif.CompositeFont')

    okButton = boxRoot.FindName('ok')
    okButton.Click += __textboxOK
    okButton.IsDefault = True
    okButton.IsCancel = True

    # ----------------- the action begins ----------------------------------------
    try:
        # load the text into the textbox
        if type(text) == type("abc"): pass
        else:
            try:
                text = "".join(text)  # convert a list or a tuple to a string
            except:
                msgbox("Exception when trying to convert "+ str(type(text)) + " to text in textbox")
                sys.exit(16)

        ##print "setting text"
        ##print text
        content.Text = text
    except:
        msgbox("Exception when trying to load the textbox.")
        sys.exit(16)

    try:
        okButton.Focus()
    except:
        msgbox("Exception when trying to put focus on okButton.")
        sys.exit(16)

    boxRoot.ShowDialog()
    __finish()
    return __replyButtonText

#-------------------------------------------------------------------
# __textboxOK
#-------------------------------------------------------------------
def __textboxOK(source, event):
    global boxRoot
    boxRoot.Hide()



#-------------------------------------------------------------------
# diropenbox
#-------------------------------------------------------------------
def diropenbox(msg=None
    , title=None
    , default=None
    ):
    """
    A dialog to get a directory name.
    Note that the msg argument, if specified, is ignored.

    Returns the name of a directory, or None if user chose to cancel.

    If the "default" argument specifies a directory name, and that
    directory exists, then the dialog box will start with that directory.
    """
    
    dialog = System.Windows.Forms.FolderBrowserDialog()
    
    dialog.Description=getFileDialogTitle(msg,title) 
    if default:
        dialog.SelectedPath = FileInfo(default).FullName
    
    state = dialog.ShowDialog()
    f = dialog.SelectedPath if state == System.Windows.Forms.DialogResult.OK else None
      
    if not f: return None
    return os.path.normpath(f)



#-------------------------------------------------------------------
# getFileDialogTitle
#-------------------------------------------------------------------
def getFileDialogTitle(msg
    , title
    ):
    if msg and title: return "%s - %s" % (title,msg)
    if msg and not title: return str(msg)
    if title and not msg: return str(title)
    return None # no message and no title

#-------------------------------------------------------------------
# class FileTypeObject for use with fileopenbox
#-------------------------------------------------------------------
class FileTypeObject:
    def __init__(self,filemask):
        if len(filemask) == 0:
            raise AssertionError('Filetype argument is empty.')
            
        self.masks = []
        
        if type(filemask) == type("abc"):  # a string
            self.initializeFromString(filemask)
            
        elif type(filemask) == type([]): # a list
            if len(filemask) < 2:
                raise AssertionError('Invalid filemask.\n'
                +'List contains less than 2 members: "%s"' % filemask)
            else:
                self.name  = filemask[-1]
                self.masks = list(filemask[:-1] )
        else:
            raise AssertionError('Invalid filemask: "%s"' % filemask)

    def __eq__(self,other):
        if self.name == other.name: return True
        return False
    
    def add(self,other):
        for mask in other.masks:
            if mask in self.masks: pass
            else: self.masks.append(mask)

    def toTuple(self):
        return (self.name,tuple(self.masks))
        
    def isAll(self):
        if self.name == "All files": return True
        return False
        
    def initializeFromString(self, filemask):
        # remove everything except the extension from the filemask
        self.ext = os.path.splitext(filemask)[1]
        if self.ext == "" : self.ext = ".*"
        if self.ext == ".": self.ext = ".*"
        self.name = self.getName()
        self.masks = ["*" + self.ext]
        
    def getName(self):
        e = self.ext
        if e == ".*"  : return "All files"
        if e == ".txt": return "Text files"
        if e == ".py" : return "Python files"
        if e == ".pyc" : return "Python files"
        if e == ".xls": return "Excel files"
        if e.startswith("."): 
            return e[1:].upper() + " files"
        return e.upper() + " files"


#-------------------------------------------------------------------
# fileopenbox
#-------------------------------------------------------------------
def fileopenbox(msg=None
    , title=None
    , default="*"
    , filetypes=None
    ):
    """
    A dialog to get a file name.
    
    About the "default" argument
    ============================
        The "default" argument specifies a filepath that (normally)
        contains one or more wildcards.
        fileopenbox will display only files that match the default filepath.
        If omitted, defaults to "*" (all files in the current directory).
    
        WINDOWS EXAMPLE::
            ...default="c:/myjunk/*.py"  
        will open in directory c:\myjunk\ and show all Python files.

        WINDOWS EXAMPLE::
            ...default="c:/myjunk/test*.py"  
        will open in directory c:\myjunk\ and show all Python files
        whose names begin with "test".
        
        
        Note that on Windows, fileopenbox automatically changes the path
        separator to the Windows path separator (backslash).

    About the "filetypes" argument
    ==============================
        If specified, it should contain a list of items,
        where each item is either::
            - a string containing a filemask          # e.g. "*.txt"        
            - a list of strings, where all of the strings except the last one
                are filemasks (each beginning with "*.",
                such as "*.txt" for text files, "*.py" for Python files, etc.).
                and the last string contains a filetype description
            
        EXAMPLE::
            filetypes = ["*.css", ["*.htm", "*.html", "HTML files"]  ]
        
    NOTE THAT
    =========

        If the filetypes list does not contain ("All files","*"),
        it will be added.

        If the filetypes list does not contain a filemask that includes
        the extension of the "default" argument, it will be added.
        For example, if     default="*abc.py"
        and no filetypes argument was specified, then
        "*.py" will automatically be added to the filetypes argument.

    @rtype: string or None
    @return: the name of a file, or None if user chose to cancel

    @arg msg: the msg to be displayed.
    @arg title: the window title
    @arg default: filepath with wildcards
    @arg filetypes: filemasks that a user can choose, e.g. "*.txt"
    """
    dialog = OpenFileDialog()
    ## filter = Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*

    initialbase, initialfile, initialdir, filetypes = fileboxSetup(default,filetypes)
    dialog.InitialDirectory = initialdir
    
    filtering = [ '%s (%s) | %s' % (x[0], ';'.join(x[1]), ';'.join(x[1])) for x in filetypes]
    dialog.Filter = '|'.join(filtering)

    #------------------------------------------------------------
    # if initialfile contains no wildcards; we don't want an
    # initial file. It won't be used anyway.
    # Also: if initialbase is simply "*", we don't want an
    # initialfile; it is not doing any useful work.
    #------------------------------------------------------------
    if (initialfile.find("*") < 0) and (initialfile.find("?") < 0):
        initialfile = None
    elif initialbase == "*":
        initialfile = None
    else:
        initialfile = FileInfo(Path.Combine(initialdir, initialfile)).FullName
    dialog.FileName = initialfile
    dialog.Title = getFileDialogTitle(msg,title)

    f = None
    if dialog.ShowDialog():
      f = dialog.FileName

    if not f: return None
    return os.path.normpath(f)

#-------------------------------------------------------------------
# filesavebox
#-------------------------------------------------------------------
def filesavebox(msg=None
    , title=None
    , default=""
    , filetypes=None
    ):
    """
    A file to get the name of a file to save.
    Returns the name of a file, or None if user chose to cancel.

    The "default" argument should contain a filename (i.e. the
    current name of the file to be saved).  It may also be empty,
    or contain a filemask that includes wildcards.
    
    The "filetypes" argument works like the "filetypes" argument to
    fileopenbox.
    """
    dialog = SaveFileDialog()


    initialbase, initialfile, initialdir, filetypes = fileboxSetup(default,filetypes)
    dialog.InitialDirectory = initialdir
    
    filtering = [ '%s (%s) | %s' % (x[0], ';'.join(x[1]), ';'.join(x[1])) for x in filetypes]
    dialog.Filter = '|'.join(filtering)

    initialfile = FileInfo(Path.Combine(initialdir, initialfile)).FullName
    dialog.FileName = initialfile
    dialog.Title = getFileDialogTitle(msg,title)
    
    f = None
    if dialog.ShowDialog():
      f = dialog.FileName
    if not f: return None
    return os.path.normpath(f)


#-------------------------------------------------------------------
#
# fileboxSetup
#
#-------------------------------------------------------------------
def fileboxSetup(default,filetypes):    
    if not default: default = os.path.join(".","*")
    initialdir, initialfile = os.path.split(default)
    if not initialdir : initialdir  = "" # was "." for current directory 
	                                 # but see http://tinesware.blogspot.co.uk/2010/03/easywpf-0393.html#comments
					 # Thanks, Anon!
    if not initialfile: initialfile = "*"
    initialbase, initialext = os.path.splitext(initialfile)
    initialFileTypeObject = FileTypeObject(initialfile)
    
    allFileTypeObject = FileTypeObject("*")
    ALL_filetypes_was_specified = False  

    if not filetypes: filetypes= []
    filetypeObjects = []

    for filemask in filetypes:
        fto = FileTypeObject(filemask)
        
        if fto.isAll(): 
            ALL_filetypes_was_specified = True # remember this
            
        if fto == initialFileTypeObject:
            initialFileTypeObject.add(fto) # add fto to initialFileTypeObject
        else:
            filetypeObjects.append(fto)

    #------------------------------------------------------------------
    # make sure that the list of filetypes includes the ALL FILES type.
    #------------------------------------------------------------------
    if ALL_filetypes_was_specified: 
        pass
    elif allFileTypeObject == initialFileTypeObject:
        pass
    else:
        filetypeObjects.insert(0,allFileTypeObject)
    #------------------------------------------------------------------
    # Make sure that the list includes the initialFileTypeObject
    # in the position in the list that will make it the default.
    # This changed between Python version 2.5 and 2.6
    #------------------------------------------------------------------ 
    if len(filetypeObjects) == 0:
        filetypeObjects.append(initialFileTypeObject)

    if initialFileTypeObject in (filetypeObjects[0], filetypeObjects[-1]):
        pass
    else:
      filetypeObjects.append(initialFileTypeObject)   
        
    filetypes = [fto.toTuple() for fto in filetypeObjects]

    return initialbase, initialfile, initialdir, filetypes

#-------------------------------------------------------------------
# utility routines
#-------------------------------------------------------------------
# These routines are used by several other functions in the EasyGui module.

def __buttonEvent(s, e):
    """
    Handle an event that is generated by a person clicking a button.
    """
    global  boxRoot, __widgetTexts, __replyButtonText
    __replyButtonText = s.Content
    __finish()

#-----------------------------------------------------------------------
#
#     class EgStore
#
#-----------------------------------------------------------------------
class EgStore:
    r"""
A class to support persistent storage.

You can use EgStore to support the storage and retrieval
of user settings for an EasyGui application.


# Example A
#-----------------------------------------------------------------------
# define a class named Settings as a subclass of EgStore
#-----------------------------------------------------------------------
class Settings(EgStore):

    def __init__(self, filename):  # filename is required
        #-------------------------------------------------
        # Specify default/initial values for variables that
        # this particular application wants to remember.
        #-------------------------------------------------
        self.userId = ""
        self.targetServer = ""

        #-------------------------------------------------
        # For subclasses of EgStore, these must be
        # the last two statements in  __init__
        #-------------------------------------------------
        self.filename = filename  # this is required
        self.restore()            # restore values from the storage file if possible



# Example B
#-----------------------------------------------------------------------
# create settings, a persistent Settings object
#-----------------------------------------------------------------------
settingsFile = "myApp_settings.txt"
settings = Settings(settingsFile)

user    = "obama_barak"
server  = "whitehouse1"
settings.userId = user
settings.targetServer = server
settings.store()    # persist the settings

# run code that gets a new value for userId, and persist the settings
user    = "biden_joe"
settings.userId = user
settings.store()


# Example C
#-----------------------------------------------------------------------
# recover the Settings instance, change an attribute, and store it again.
#-----------------------------------------------------------------------
settings = Settings(settingsFile)
settings.userId = "vanrossum_g"
settings.store()

"""
    def __init__(self, filename):  # obtaining filename is required
        raise NotImplementedError()

    def restore(self):
        """
        Set the values of whatever attributes are recoverable
        from the pickle file.

        Populate the attributes (the __dict__) of the EgStore object
        from     the attributes (the __dict__) of the pickled object.

        If the pickled object has attributes that have been initialized
        in the EgStore object, then those attributes of the EgStore object
        will be replaced by the values of the corresponding attributes
        in the pickled object.

        If the pickled object is missing some attributes that have
        been initialized in the EgStore object, then those attributes
        of the EgStore object will retain the values that they were
        initialized with.

        If the pickled object has some attributes that were not
        initialized in the EgStore object, then those attributes
        will be ignored.

        IN SUMMARY:

        After the recover() operation, the EgStore object will have all,
        and only, the attributes that it had when it was initialized.

        Where possible, those attributes will have values recovered
        from the pickled object.
        """
        if not os.path.exists(self.filename): return self
        if not os.path.isfile(self.filename): return self

        try:
            f = open(self.filename)
            unpickledObject = pickle.load(f)
            f.close()

            for key in list(self.__dict__.keys()):
                default = self.__dict__[key]
                self.__dict__[key] = unpickledObject.__dict__.get(key,default)
        except:
            pass

        return self

    def store(self):
        """
        Save the attributes of the EgStore object to a pickle file.
        Note that if the directory for the pickle file does not already exist,
        the store operation will fail.
        """
        f = open(self.filename, "w")
        pickle.dump(self, f)
        f.close()


    def kill(self):
        """
        Delete my persistent file (i.e. pickle file), if it exists.
        """
        if os.path.isfile(self.filename):
            os.remove(self.filename)
        return

    def __str__(self):
        """
        return my contents as a string in an easy-to-read format.
        """
        # find the length of the longest attribute name
        longest_key_length = 0
        keys = []
        for key in self.__dict__.keys():
            keys.append(key)
            longest_key_length = max(longest_key_length, len(key))

        keys.sort()  # sort the attribute names
        lines = []
        for key in keys:
            value = self.__dict__[key]
            key = key.ljust(longest_key_length)
            lines.append("%s : %s\n" % (key,repr(value))  )
        return "".join(lines)  # return a string showing the attributes




#-----------------------------------------------------------------------
#
# test/demo easygui
#
#-----------------------------------------------------------------------
def egdemo():
    """
    Run the EasyGui demo.
    """
    # clear the console
    writeln("\n" * 100)
    
    # ============================= define a code snippet =========================
    code_snippet = ("dafsdfa dasflkj pp[oadsij asdfp;ij asdfpjkop asdfpok asdfpok asdfpok"*3) +"\n"+\
"""# here is some dummy Python code
for someItem in myListOfStuff:
    do something(someItem)
    do something()
    do something()
    if somethingElse(someItem):
        doSomethingEvenMoreInteresting()

"""*16
    #======================== end of code snippet ==============================

    #================================= some text ===========================
    text_snippet = ((\
"""It was the best of times, and it was the worst of times.  The rich ate cake, and the poor had cake recommended to them, but wished only for enough cash to buy bread.  The time was ripe for revolution! """ \
*5)+"\n\n")*10

    #===========================end of text ================================

    intro_message = '''
Pick the kind of box that you wish to demo.
 * IronPython version %s
 * EasyWPF version %s''' % (sys.version, egversion)
 
    #========================================== END DEMONSTRATION DATA


    while 1: # do forever
        choices = [
            "msgbox",
            "buttonbox",
            "buttonbox(image) -- a buttonbox that displays an image",
            "choicebox",
            "multchoicebox",
            "textbox",
            "ynbox",
            "ccbox",
            "enterbox",
            "enterbox(image) -- an enterbox that displays an image",
            "exceptionbox",
            "codebox",
            "integerbox",
            "boolbox",
            "indexbox",
            "filesavebox",
            "fileopenbox",
            "passwordbox",
            "multenterbox",
            "multpasswordbox",
            "diropenbox",
            "About EasyGui",
            " Help"
            ]
        choice = choicebox(msg=intro_message
            , title="EasyWPF " + egversion
            , choices=choices)

        if not choice: return

        reply = choice.split()

        if   reply[0] == "msgbox":
            reply = msgbox("short msg", "This is a long title")
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "About":
            reply = abouteasywpf()

        elif reply[0] == "Help":
            _demo_help()

        elif reply[0] == "buttonbox":
            reply = buttonbox()
            writeln("Reply was: %s" % repr(reply))

            title = "Demo of Buttonbox with many, many buttons!"
            msg = "This buttonbox shows what happens when you specify too many buttons."
            reply = buttonbox(msg=msg, title=title, choices=choices)
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "buttonbox(image)":
            _demo_buttonbox_with_image()

        elif reply[0] == "boolbox":
            reply = boolbox()
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "enterbox":
            image = "python_and_check_logo.gif"
            message = "Enter the name of your best friend."\
                      "\n(Result will be stripped.)"
            reply = enterbox(message, "Love!", "     Suzy Smith     ")
            writeln("Reply was: %s" % repr(reply))

            message = "Enter the name of your best friend."\
                      "\n(Result will NOT be stripped.)"
            reply = enterbox(message, "Love!", "     Suzy Smith     ",strip=False)
            writeln("Reply was: %s" % repr(reply))

            reply = enterbox("Enter the name of your worst enemy:", "Hate!")
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "enterbox(image)":
            image = "python_and_check_logo.gif"
            message = "What kind of snake is this?"
            reply = enterbox(message, "Quiz",image=image)
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "exceptionbox":
            try:
                thisWillCauseADivideByZeroException = 1/0
            except:
                exceptionbox()
            
        elif reply[0] == "integerbox":
            reply = integerbox(
                "Enter a number between 3 and 333",
                "Demo: integerbox WITH a default value",
                222, 3, 333)
            writeln("Reply was: %s" % repr(reply))

            reply = integerbox(
                "Enter a number between 0 and 99",
                "Demo: integerbox WITHOUT a default value"
                )
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "diropenbox" : _demo_diropenbox()
        elif reply[0] == "fileopenbox": _demo_fileopenbox()
        elif reply[0] == "filesavebox": _demo_filesavebox()

        elif reply[0] == "indexbox":
            title = reply[0]
            msg   =  "Demo of " + reply[0]
            choices = ["Choice1", "Choice2", "Choice3", "Choice4"]
            reply = indexbox(msg, title, choices)
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "passwordbox":
            reply = passwordbox("Demo of password box WITHOUT default"
                + "\n\nEnter your secret password", "Member Logon")
            writeln("Reply was: %s" % str(reply))

            reply = passwordbox("Demo of password box WITH default"
                + "\n\nEnter your secret password", "Member Logon", "alfie")
            writeln("Reply was: %s" % str(reply))

        elif reply[0] == "multenterbox":
            msg = "Enter your personal information"
            title = "Credit Card Application"
            fieldNames = ["Name","Street Address","City","State","ZipCode"]
            fieldValues = []  # we start with blanks for the values
            fieldValues = multenterbox(msg,title, fieldNames)

            # make sure that none of the fields was left blank
            while 1:
                if fieldValues == None: break
                errmsg = ""
                for i in range(len(fieldNames)):
                    if fieldValues[i].strip() == "":
                        errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
                if errmsg == "": break # no problems found
                fieldValues = multenterbox(errmsg, title, fieldNames, fieldValues)

            writeln("Reply was: %s" % str(fieldValues))

        elif reply[0] == "multpasswordbox":
            msg = "Enter logon information"
            title = "Demo of multpasswordbox"
            fieldNames = ["Server ID", "User ID", "Password"]
            fieldValues = []  # we start with blanks for the values
            fieldValues = multpasswordbox(msg,title, fieldNames)

            # make sure that none of the fields was left blank
            while 1:
                if fieldValues == None: break
                errmsg = ""
                for i in range(len(fieldNames)):
                    if fieldValues[i].strip() == "":
                        errmsg = errmsg + ('"%s" is a required field.\n\n' % fieldNames[i])
                if errmsg == "": break # no problems found
                fieldValues = multpasswordbox(errmsg, title, fieldNames, fieldValues)

            writeln("Reply was: %s" % str(fieldValues))

        elif reply[0] == "ynbox":
            title = "Demo of ynbox"
            msg = "Were you expecting the Spanish Inquisition?"
            reply = ynbox(msg, title)
            writeln("Reply was: %s" % repr(reply))
            if reply:
                msgbox("NOBODY expects the Spanish Inquisition!", "Wrong!")

        elif reply[0] == "ccbox":
            title = "Demo of ccbox"
            msg = "Fix EasyGUI bug by initializing mag"
            reply = ccbox(msg,title)
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "choicebox":
            title = "Demo of choicebox"
            longchoice = "This is an example of a very long option which you may or may not wish to choose."*2
            listChoices = ["nnn", "ddd", "eee", "fff", "aaa", longchoice
                    , "aaa", "bbb", "ccc", "ggg", "hhh", "iii", "jjj", "kkk", "LLL", "mmm" , "nnn", "ooo", "ppp", "qqq", "rrr", "sss", "ttt", "uuu", "vvv"]

            msg = "Pick something. " + ("A wrapable sentence of text ?! "*30) + "\nA separate line of text."*6
            reply = choicebox(msg=msg, choices=listChoices)
            writeln("Reply was: %s" % repr(reply))

            msg = "Pick something. "
            reply = choicebox(msg=msg, title=title, choices=listChoices)
            writeln("Reply was: %s" % repr(reply))

            msg = "Pick something. "
            reply = choicebox(msg="The list of choices is empty!", choices=[])
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "multchoicebox":
            listChoices = ["aaa", "bbb", "ccc", "ggg", "hhh", "iii", "jjj", "kkk"
                , "LLL", "mmm" , "nnn", "ooo", "ppp", "qqq"
                , "rrr", "sss", "ttt", "uuu", "vvv"]

            msg = "Pick as many choices as you wish."
            reply = multchoicebox(msg,"Demo of multchoicebox", listChoices)
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "textbox":
            title = "Demo of textbox"
            msg = "Here is some sample text. " * 16
            reply = textbox(msg, "Text Sample", text_snippet)
            writeln("Reply was: %s" % repr(reply))

        elif reply[0] == "codebox":
            msg = "Here is some sample code. " * 16
            reply = codebox(msg, "Code Sample", code_snippet)
            writeln("Reply was: %s" % repr(reply))

        else:
            msgbox("Choice\n\n" + choice + "\n\nis not recognized", "Program Logic Error")
            return



def _demo_buttonbox_with_image():
    image = "python_and_check_logo.gif"

    msg   = "Pretty nice, huh!"
    reply=msgbox(msg,image=image, ok_button="Wow!")
    writeln("Reply was: %s" % repr(reply))

    msg   = "Do you like this picture?"
    choices = ["Yes","No","No opinion"]

    reply=buttonbox(msg,image=image,choices=choices)
    writeln("Reply was: %s" % repr(reply))

    image = os.path.normpath("python_and_check_logo.png")
    reply=buttonbox(msg,image=image, choices=choices)
    writeln("Reply was: %s" % repr(reply))

    image = os.path.normpath("zzzzz.gif")
    reply=buttonbox(msg,image=image, choices=choices)
    writeln("Reply was: %s" % repr(reply))


def _demo_help():
    from StringIO import StringIO
    savedStdout = sys.stdout    # save the sys.stdout file object
    sys.stdout = capturedOutput = StringIO()
    help("EasyWPF")
    sys.stdout = savedStdout   # restore the sys.stdout file object
    codebox("EasyWPF Help",text=capturedOutput.getvalue())

def _demo_filesavebox():    
    filename = "myNewFile.txt"
    title = "File SaveAs"
    msg ="Save file as:"

    f = filesavebox(msg,title,default=filename)
    writeln("You chose to save file: %s" % f)

def _demo_diropenbox(): 
    title = "Demo of diropenbox"
    msg = "Pick the directory that you wish to open."
    d = diropenbox(msg, title)
    writeln("You chose directory...: %s" % d)

    d = diropenbox(msg, title,default="./")
    writeln("You chose directory...: %s" % d)
    
    d = diropenbox(msg, title,default="c:/")
    writeln("You chose directory...: %s" % d)
    
    
def _demo_fileopenbox():        
    msg  = "Python files"
    title = "Open files"
    default="*.py"
    f = fileopenbox(msg,title,default=default)
    writeln("You chose to open file: %s" % f)

    default="./*.gif"
    filetypes = ["*.jpg",["*.zip","*.tgs","*.gz", "Archive files"],["*.htm", "*.html","HTML files"]]
    f = fileopenbox(msg,title,default=default,filetypes=filetypes)
    writeln("You chose to open file: %s" % f)
    
    """#deadcode -- testing ----------------------------------------
    f = fileopenbox(None,None,default=default)
    writeln("You chose to open file: %s" % f)

    f = fileopenbox(None,title,default=default)
    writeln("You chose to open file: %s" % f)

    f = fileopenbox(msg,None,default=default)
    writeln("You chose to open file: %s" % f)

    f = fileopenbox(default=default)
    writeln("You chose to open file: %s" % f)

    f = fileopenbox(default=None)
    writeln("You chose to open file: %s" % f)
    #----------------------------------------------------deadcode """


def _dummy():
    pass

EASYWPF_ABOUT_INFORMATION = '''
========================================================================
0.3.93(2010-Mar-28)
========================================================================

ENHANCEMENTS
------------------------------------------------------

 * Make the WNDPROC callback for handling the Composition Changed message
   actually set the "handled" parameter

========================================================================
0.2.93(2010-Mar-08)
========================================================================

ENHANCEMENTS
------------------------------------------------------

 * Complete port to apparent parity with EasyGui 0.93

========================================================================
0.1.93(2010-Mar-07)
========================================================================

ENHANCEMENTS
------------------------------------------------------

 * First partial implementation against WPF of EasyGui 0.93
'''

def abouteasywpf():
    """
    shows the easywpf revision history
    """
    codebox("About EasyWPF\n"+egversion,"EasyWPF",EASYWPF_ABOUT_INFORMATION)
    return None



if __name__ == '__main__':
    if len(sys.argv) == 1:
        egdemo()
    else:
        # test the new root feature 
        xaml = '''<Window
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    Title="Window3" Height="300" Width="300">
    <Grid>
        <TextBox Name="textBox1" />
    </Grid>
</Window>'''        
        
        root = LoadXaml(xaml)
        root.FindName('textBox1').Text = """This is a test of a main WPF window in which we will place an easygui msgbox.
                It will be an interesting experiment.\n\n"""
        root.Show()
        msgbox("this is a test of passing in boxRoot", root=root)
        msgbox("this is a second test of passing in boxRoot", root=root)
        
        reply = enterbox("Enter something", root=root)
        writeln("You wrote:", reply)

        reply = enterbox("Enter something else", root=root)
        writeln("You wrote:", reply)
        root.Hide()
        root.Close()
        root = None 
