﻿namespace Tinesware.FSharp

open System
open System.Collections.Generic
open Should

module fhould =
  let inline (|->) x y =
    ObjectAssertExtensions.ShouldBeGreaterThan(x, y)

  let inline (|->>) (x:'a) (y:'a) (z:IComparer<'a>) =
    ObjectAssertExtensions.ShouldBeGreaterThan(x, y, z)

  let inline (|->=) x y =
    ObjectAssertExtensions.ShouldBeGreaterThanOrEqualTo(x, y)

  let inline (|->>=) (x:'a) (y:'a) (z:IComparer<'a>) =
    ObjectAssertExtensions.ShouldBeGreaterThanOrEqualTo(x, y, z)

  let inline (|-<>) x (y, z) =
    ObjectAssertExtensions.ShouldBeInRange(x, y, z)

  let inline (|-<<>>) (x:'a) (y:'a * 'a) (z:IComparer<'a>) =
    ObjectAssertExtensions.ShouldBeInRange(x, fst y, snd y, z)

  let inline (|-<) x y =
    ObjectAssertExtensions.ShouldBeLessThan(x, y)

  let inline (|-<<) (x:'a) (y:'a) (z:IComparer<'a>) =
    ObjectAssertExtensions.ShouldBeLessThan(x, y, z)

  let inline (|-<=) x y =
    ObjectAssertExtensions.ShouldBeLessThanOrEqualTo(x, y)

  let inline (|-<<=) (x:'a) (y:'a) (z:IComparer<'a>) =
    ObjectAssertExtensions.ShouldBeLessThanOrEqualTo(x, y, z)

  let inline (!-/) x =
    ObjectAssertExtensions.ShouldBeNull(x)

  let inline (|-===) x y =
    ObjectAssertExtensions.ShouldBeSameAs(x, y)

  let inline (|-@) x y =
    ObjectAssertExtensions.ShouldBeType(x, y)

  let inline (|-<@) x (y:Type) =
    ObjectAssertExtensions.ShouldImplement(x, y)

  let inline (|-<@%) x y z =
    ObjectAssertExtensions.ShouldImplement(x, y, z)

  let inline (|-=) x y =
    ObjectAssertExtensions.ShouldEqual(x, y)

  let inline (|-=%) x y (s:String)=
    ObjectAssertExtensions.ShouldEqual(x, y, s)

  let inline (|-==) (x:'a) (y:'a) (z:IEqualityComparer<'a>)=
    ObjectAssertExtensions.ShouldEqual(x, y, z)

  let inline (|-><) x (y, z) =
    ObjectAssertExtensions.ShouldNotBeInRange(x, y, z)

  let inline (|->><<) (x:'a) (y:'a * 'a) (z:IComparer<'a>) =
    ObjectAssertExtensions.ShouldNotBeInRange(x, fst y, snd y, z)

  let inline (!-?) x =
    ObjectAssertExtensions.ShouldNotBeNull(x) |> ignore

  let inline (|-?%) x y =
    ObjectAssertExtensions.ShouldNotBeNull(x, y) |> ignore

  let inline (|-!==) x y =
    ObjectAssertExtensions.ShouldNotBeSameAs(x, y)

  let inline (|-!@) x y =
    ObjectAssertExtensions.ShouldNotBeType(x, y)

  let inline (|-!=) x y=
    ObjectAssertExtensions.ShouldNotEqual(x, y)

  let inline (|-!!=) (x:'a) (y:'a) (z:IEqualityComparer<'a>)=
    ObjectAssertExtensions.ShouldNotEqual(x, y, z)

  let shouldThrow<'a when 'a :> Exception> f =
    ActionAssertionExtensions.ShouldThrow<'a>(new Should.Core.Assertions.Assert.ThrowsDelegate(f))