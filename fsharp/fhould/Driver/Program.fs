﻿
open System
open System.Collections.Generic

open Should
open Tinesware.FSharp.fhould

[<EntryPoint>]
let main argv = 
 try
  let zero = 0
  let one = 1
  let two = 2
  let nil : obj = null
  let some = new Object()
  let same = some
  let other = new Object()

  one |-> 0
  shouldThrow<Should.Core.Exceptions.GreaterThanException> (fun() -> one |-> 2)

  one |->> 0 <| Comparer<int>.Default
  shouldThrow<Should.Core.Exceptions.GreaterThanException> (fun() -> one |->> 2 <| Comparer<int>.Default)

  one |->= 0
  shouldThrow<Should.Core.Exceptions.GreaterThanOrEqualException> (fun() -> one |->= 2)

  one |->>= 0 <| Comparer<int>.Default
  shouldThrow<Should.Core.Exceptions.GreaterThanOrEqualException> (fun() -> one |->>= 2 <| Comparer<int>.Default)

  one |-<> (0, 2)
  shouldThrow<Should.Core.Exceptions.InRangeException> (fun() -> zero |-<> (1, 2))

  one |-<<>> (0, 2) <| Comparer<int>.Default
  shouldThrow<Should.Core.Exceptions.InRangeException> (fun() -> zero |-<<>> (1, 2) <| Comparer<int>.Default)

  one |-< 2
  shouldThrow<Should.Core.Exceptions.LessThanException> (fun() -> one |-< 0)

  one |-<< 2 <| Comparer<int>.Default
  shouldThrow<Should.Core.Exceptions.LessThanException> (fun() -> one |-<< 0 <| Comparer<int>.Default)

  one |-<= 2
  shouldThrow<Should.Core.Exceptions.LessThanOrEqualException> (fun() -> one |-<= 0)

  one |-<<= 2 <| Comparer<int>.Default
  shouldThrow<Should.Core.Exceptions.LessThanOrEqualException> (fun() -> one |-<<= 0 <| Comparer<int>.Default)

  !-/ nil
  shouldThrow<Should.Core.Exceptions.NullException> (fun() -> !-/ some)

  some |-=== same
  shouldThrow<Should.Core.Exceptions.SameException> (fun() -> some |-=== other)

  "string" |-@ typeof<String>
  shouldThrow<Should.Core.Exceptions.IsTypeException> (fun() -> some |-@ typeof<String>)

  "string" |-<@ typeof<System.Collections.IEnumerable>
  shouldThrow<Should.Core.Exceptions.IsAssignableFromException> (fun() -> some |-<@ typeof<System.Collections.IEnumerable>)

  "string" |-<@% typeof<System.Collections.IEnumerable> <| "message"
  shouldThrow<Should.Core.Exceptions.IsAssignableFromException> (fun() -> some |-<@% typeof<System.Collections.IEnumerable> <| "message")

  one |-= 1
  shouldThrow<Should.Core.Exceptions.EqualException> (fun() -> one |-= 2)

  one |-=% 1 <| "message"
  shouldThrow<Should.Core.Exceptions.EqualException> (fun() -> one |-=% 2 <| "message")

  zero |->< (1, 2)
  shouldThrow<Should.Core.Exceptions.NotInRangeException> (fun() -> one |->< (0, 2))

  zero |->><< (1, 2) <| Comparer<int>.Default
  shouldThrow<Should.Core.Exceptions.NotInRangeException> (fun() -> one |->><< (0, 2) <| Comparer<int>.Default)

  !-? some
  shouldThrow<Should.Core.Exceptions.NotNullException> (fun() -> !-? nil)

  some |-?% "message"
  shouldThrow<Should.Core.Exceptions.NotNullException> (fun() -> nil |-?% "message")

  some |-!== other
  shouldThrow<Should.Core.Exceptions.NotSameException> (fun() -> some |-!== same)

  some |-!@ typeof<String>
  shouldThrow<Should.Core.Exceptions.IsNotTypeException> (fun() -> "string" |-!@ typeof<String>)

  one |-!= 2
  shouldThrow<Should.Core.Exceptions.NotEqualException> (fun() -> one |-!= 1)

  one |-!!= 2 <| EqualityComparer<int>.Default
  shouldThrow<Should.Core.Exceptions.NotEqualException> (fun() -> one |-!!= 1 <| EqualityComparer<int>.Default)

  let ok = true
  let nok = false

  nok.ShouldBeFalse()
  ok.ShouldBeTrue()

  let seq0 = []
  let seq1 = [ 1 ]
  seq0.ShouldBeEmpty()
  seq1.ShouldNotBeEmpty()
  seq1.ShouldContain(1)
  seq1.ShouldNotContain(2)
  Console.WriteLine("Success!")
 with
 | ex -> Console.WriteLine(ex)

 //Console.ReadLine().Length
 0

