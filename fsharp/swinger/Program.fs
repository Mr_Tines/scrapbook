﻿open java.awt
open javax.swing

let createAndShowGUI() =
        let laf = new com.ms.vjsharp.swing.plaf.windows.WindowsLookAndFeel()
        UIManager.setLookAndFeel(laf) 
        try 
            //Create and set up the window.
            let frame = JFrame("HelloWorldSwing");
            //frame.setDefaultCloseOperation(3) // exit on close

            //Add the ubiquitous "Hello World" label.
            let label = new JLabel("Hello World")
            frame.add(label) |> ignore

            //Display the window.
            frame.pack()
            frame.setVisible(true)
        with
        | x -> printfn "%A" <| x.ToString()
               printfn "%A" <| x.StackTrace.ToString()
               printfn "%A" <| x.InnerException


createAndShowGUI()
()
