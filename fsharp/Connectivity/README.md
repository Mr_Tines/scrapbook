### What are we trying to do here?

A little program to put a systray icon to show a clear distinction between merely having negotiated WiFi and actually having internet, unlike the Win 7 or later systray notifications -- and because my WiFi link used to resume from hibernation immediately under Vista, but now I have to wait for it to renegotiate from scratch under later OS versions.

The icon is dark with a red mark when there is no connection, grey with a yellow mark when there is some connection but not yet internet, and coloured with no mark when there is internet.  Hover text will supply some finer grained information about the greyed out state (you may have local network access, but not internet, for example).

### What license applies to this code?

This code is released under the [WTFPL](http://www.wtfpl.net/).

### I don't know how to build a program but I want to use it

The build, as of [commit c2e144b](https://bitbucket.org/Mr_Tines/scrapbook/commits/c2e144bd501aa1ed9fe61b01771a6980132c8c25), is [available as a zipped download](https://bitbucket.org/Mr_Tines/scrapbook/downloads/Connectivity.zip).

#### Hashes of the zip file are as follows

* MD5     : 2bb4 91f3 190c 4c1a bff0 0d56 fa76 849c
* SHA     : 737b c42f 0b0f b7ab 56e7 810c f060 1170 44f7 6a83
* SHA2-256: 10ac 46b6 8775 c01f 9785 e5b9 0994 e3bc 88ac ee42 c75a 7191 5fd2 109f 503c 87f8

#### Notes

* you just need to unzip it somewhere (no installer); the 3 files -- one .exe and 2 .dll -- inside need to live together.
* It does assume you have .net 4.0 or later installed (but that should come as part of the operating system for Windows 10).
* You'll need to create a shortcut manually to put on the desktop or [add to the start menu](http://www.tenforums.com/tutorials/10529-all-apps-start-menu-add-remove-items-windows-10-a.html) or [to start automatically](http://www.howtogeek.com/208224/how-to-add-programs-files-and-folders-to-system-startup-in-windows-8.1/)
* to stop it, right-click the icon and click "Exit" on the menu
* Overnight hibernation means that the link has to be renegotiated from scratch, but for shorter breaks (maybe up to 6 hours), I have had it come up immediately, so not quite as good as Vista behaviour -- YMMV
* it takes about 700K memory and negligible CPU
