﻿open System
open System.IO
open System.IO.Compression
open System.Text

open java.io
open java.util.zip

open Should

type DisposableInputStream (jStream : java.io.InputStream) =
  interface IDisposable with
    member self.Dispose() =
      jStream.close()
  member self.Dispose() =
    (self :> IDisposable).Dispose()

type DisposableOutputStream (jStream : java.io.OutputStream) =
  interface IDisposable with
    member self.Dispose() =
      jStream.close()
  member self.Dispose() =
    (self :> IDisposable).Dispose()


let JavaInputStreamToJavaByteSeq (stream : InputStream) =
  // can't do try-with inside a seq expression
  let safeRead (source:InputStream) xbuffer =
    try
      source.read(xbuffer, 0, xbuffer.Length)
    with
    | :? IOException as x -> printfn "%A" x.Message
                             -1
  seq { use d = new DisposableInputStream(stream)

        let xbuffer = Array.create 4096 0y
        let loop = ref true
        // take big bites
        while !loop do
          let x = safeRead stream xbuffer
          if x > 0 then yield! xbuffer |> Seq.take x
          loop := x >= 0 }



// Java I/O read of whole file into signed byte array
let readFileToSByteArray (name: string) =
  seq { let source = new FileInputStream(name)
        yield! JavaInputStreamToJavaByteSeq source } |> Seq.toArray
  
let fromJavaByteArray (buffer : array<sbyte>) = buffer |> Array.map byte
let toJavaByteArray (buffer : array<byte>) = buffer |> Array.map sbyte
    
let update_adler adler buffer =
  let pair = (adler &&& 0xffff, (adler >>> 16) &&& 0xffff)
  let BASE = 65521
  let (p1, p2) = buffer 
                 |> Array.fold (fun (s1, s2) b -> let tmp = (s1 + int b) % BASE
                                                  ( tmp, (tmp + s2) % BASE )) pair
  (p2 <<< 16) + p1

// All J# deflation of an array<sbyte> -> array<sbyte>
let jdeflate buffer = 
  let doDeflate () =
    let sink0 = new ByteArrayOutputStream()
    use d0 = new DisposableOutputStream(sink0)
    let sink = new DeflaterOutputStream(sink0)
    use d = new DisposableOutputStream(sink)
    sink.write(buffer, 0, buffer.Length)
    sink0

  (doDeflate ()).toByteArray()

// All J# inflation of an array<sbyte> -> array<sbyte>
let jinflate jbuffer = 
  seq { let source0 = new ByteArrayInputStream(jbuffer)
        use d0 = new DisposableInputStream(source0)
        let source = new InflaterInputStream(source0)
        yield! JavaInputStreamToJavaByteSeq source } |> Seq.toArray

// All .net deflation of an array<byte> -> array<byte> 
let ndeflate ubuffer =
  let flushDeflate () = 
    use sink0 = new MemoryStream()
    use sink = new DeflateStream(sink0, CompressionMode.Compress)
    sink.Write(ubuffer, 0, ubuffer.Length)
    // overkill the flush and close here
    sink.Flush()
    sink0.Flush()
    sink0
  (flushDeflate ()).ToArray()

// All .net inflation of an array<byte> -> array<byte> 
// Assumes a pure deflate stream in the buffer 
let ninflate (buffer : array<byte>) =
  seq { use mem0 = new MemoryStream(buffer)
        use inflate = new DeflateStream(mem0, CompressionMode.Decompress)
        let loop = ref true
        while !loop do
          let x = inflate.ReadByte()
          loop := x >= 0
          if !loop then yield byte x } |> Seq.toArray

// remove the J# ZLIB wrapper to reveal the inner deflate stream
// Use this before ninflate
let stripZLIBwrapper (buffer:array<byte>) =
  let size = buffer.Length - 6
  buffer |> Seq.skip 2 |> Seq.take size |> Seq.toArray

let ninflateZLIB = stripZLIBwrapper >> ninflate

let ndeflateZLIB (buffer:array<byte>) =
  let adler = update_adler 1 buffer
  let deflated = ndeflate buffer

  // now inflate
  let header = [| 120uy ; // 0111 0100 = 120 : 32 kbit window, Deflate
                  156uy |] // # 10 0 ????? = 128 + x : default compress, no dict, checksum
  let tail = [| byte (adler>>>24) ; byte (adler>>>16) ; byte (adler>>>8) ; byte (adler) |]

  Seq.append header (Seq.append deflated tail)
  |> Seq.toArray

let ninflateZLIBFull (deflated:array<byte>) =
  // check the ZLIB header -- expect 120, 156 actually (120,-100)
  let first = (int deflated.[0] &&& 0xff)
  let second = (int deflated.[1] &&& 0xff)
  Should.ObjectAssertExtensions.ShouldEqual((first, second), (120,156))

  // Extract the Adler32 checksum for the content
  let i = deflated.Length-4
  let x = ((int deflated.[i] &&& 0xff) <<< 24) ||| ((int deflated.[i+1]&&&0xff) <<< 16) ||| ((int deflated.[i+2]&&&0xff) <<< 8) ||| (int deflated.[i+3]&&&0xff)

  // reflate ZLIB in .net
  let output = deflated
               |> ninflateZLIB
  let adler = update_adler 1 output
  Should.ObjectAssertExtensions.ShouldEqual(x, adler)

  output

//==========================================

[<EntryPoint>]
let main argv = 
  // select a file
  let n1 = @"D:\Steve\hg\scrapbook\fsharp\Zlib\Zlib\Program.fs"
  let n2 = @"C:\Users\steve\hg\scrapbook\fsharp\Zlib\ZLIB\Program.fs"

  let name = if System.IO.File.Exists(n1) then n1
             else n2 //...
             
  // make signed, unsigned buffers and string
  let buffer = readFileToSByteArray name
  let ubuffer = fromJavaByteArray buffer
  let instring = Encoding.Default.GetString(ubuffer)

  // compute the Adler32 checksum
  let adler = update_adler 1 ubuffer
 
  // deflate in Java -- this is ZLIB format
  let deflated = jdeflate(buffer)
  let checksum = update_adler 1 (deflated |> fromJavaByteArray)
  let output = deflated
               |> fromJavaByteArray
               |> ninflateZLIBFull

  // compare with input
  Should.ObjectAssertExtensions.ShouldEqual(output.Length, buffer.Length)
  let outstring = Encoding.Default.GetString(output)
  Should.ObjectAssertExtensions.ShouldEqual(outstring, instring)

  // now the other way around
  let jbuffer = ubuffer
                |> ndeflateZLIB
                |> toJavaByteArray
  Should.ObjectAssertExtensions.ShouldEqual(jbuffer.Length, deflated.Length)

  let validate = Seq.forall2 (fun x y -> x = y) deflated jbuffer 
  Should.ObjectAssertExtensions.ShouldEqual(validate, true)

  let checksum2 = update_adler 1 (jbuffer |> fromJavaByteArray)
  Should.ObjectAssertExtensions.ShouldEqual(checksum2, checksum)

  let reflated = jbuffer
                 |> jinflate
                 |> fromJavaByteArray

  Should.ObjectAssertExtensions.ShouldEqual(reflated.Length, buffer.Length)
  let outstring = Encoding.Default.GetString(reflated)
  Should.ObjectAssertExtensions.ShouldEqual(outstring, instring)
  0 // return an integer exit code
