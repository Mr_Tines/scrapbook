namespace cmdlet

open System.Management.Automation

[<Cmdlet(VerbsCommunications.Write, "Hello")>]
[<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.PowerShell", "PS1101:AllCmdletsShouldAcceptPipelineInput", Justification = "No valid input")>]
type WriteHelloCommand() =
  inherit Cmdlet()

  override self.ProcessRecord() =
        self.WriteObject("Hello, World!")
