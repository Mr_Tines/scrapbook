﻿open System
open Gtk
open Glade

let mutable click_count = 0
 
type Handler() = class
       [<Widget>]
       [<DefaultValue(true)>]
       val mutable button1 : Button 
       
       [<Widget>]
       [<DefaultValue(true)>]
       val mutable label1 : Label
       
       [<Widget>]
       [<DefaultValue(true)>]
       val mutable window1 : Window
end

let OnClick (h : Handler) =
  //h.label1.Text <- "Mono" -- original example's code
  click_count <- click_count + 1
  // Console.WriteLine("Button Click {0}", click_count)
  h.label1.Text <- String.Format("Button Click {0}", click_count)
  
let OnDelete (args:DeleteEventArgs) =
  Application.Quit()
  args.RetVal <- true  
 
[<EntryPoint>]
let main a =
   Application.Init()
   // Developed with Glade 3.8.3 http://ftp.gnome.org/pub/GNOME/binaries/win32/glade/3.8/glade-3-8-3-installer.exe
   // Needs libglade project file format
   let gxml = new Glade.XML (null, "gui.glade", "window1", null)
   let handler = new Handler()
   gxml.Autoconnect (handler)

   handler.button1.Clicked
   |> Event.add (fun _ -> OnClick handler)

   handler.window1.DeleteEvent
   |> Event.add OnDelete

   handler.window1.ShowAll()
   Application.Run()
 
   0 // needs an int return