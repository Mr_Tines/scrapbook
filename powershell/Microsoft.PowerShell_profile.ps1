function Get-Batchfile ($file) {
    $cmd = "`"$file`" & set"
    cmd /c $cmd | Foreach-Object {
        $p, $v = $_.split('=')
        Set-Item -path env:$p -value $v
    }
}

function VsVars32($version = "14.0")
{
    if([int]$version -gt 14.0)
    {
        if([int]$version -gt 15.0) { $key = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\f948e280"}
        else { $key = "HKLM:\SOFTWARE\Microsoft\VisualStudio\SxS\VS7" }

        if (Test-Path $key) {
            $VsKey = get-ItemProperty $key
        }

        if (-not $VsKey)
        {
            if([int]$version -gt 16.0) { $key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\61ffa331"}
            else {
              if([int]$version -gt 15.0) { $key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\f948e280"}
              else { $key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\VisualStudio\SxS\VS7" }
            }
            $VsKey = get-ItemProperty $key
        }

        if([int]$version -gt 15.0) { $VsInstallPath = $VsKey.InstallLocation }
        else { $VsInstallPath = $VsKey.$version }
        $VcTools = Join-Path $VsInstallPath "Common7\Tools"
        $BatchFile = Join-Path $VcTools VsDevCmd.bat
    }
    else {
        $key = "HKLM:\SOFTWARE\Microsoft\VisualStudio\" + $version    
        $VsKey = get-ItemProperty $key
        
        if (-not $VsKey)
        {
            $key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\VisualStudio\" + $version    
            $VsKey = get-ItemProperty $key
        }

        $VsInstallPath = [System.IO.Path]::GetDirectoryName($VsKey.InstallDir)
        $VsToolsDir = [System.IO.Path]::GetDirectoryName($VsInstallPath)
        $VsToolsDir = [System.IO.Path]::Combine($VsToolsDir, "Tools")
        $BatchFile = [System.IO.Path]::Combine($VsToolsDir, "vsvars32.bat")
    }

    Get-Batchfile $BatchFile
    [System.Console]::Title = "Visual Studio " + $version + " Powershell Core"
}

$currentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent() )

function prompt 
{
  if ($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
  {
        if( !$host.UI.RawUI.WindowTitle.StartsWith( "Administrator: " ) )
        { $Host.UI.RawUI.WindowTitle = "Administrator: " + $host.UI.RawUI.WindowTitle }
  }
  "PS: $(get-location)`r`n$(get-date)$($GLOBAL:PromptTrail)"
}

& {
    $p = Get-process -id $pid 
    if ($currentPrincipal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator ))
    {
	$GLOBAL:PromptTrail = "#"
    (get-host).UI.RawUI.Backgroundcolor="DarkRed"
    #(get-host).UI.RawUI.Backgroundcolor="Black"
    if ($p.Parent.Parent) {. (Join-Path -Path (Split-Path -Parent -Path $PROFILE) -ChildPath 'Set-SolarizedDarkColorDefaults.ps1') }
    }
    else
    {
    $GLOBAL:PromptTrail = ">"
     #(get-host).UI.RawUI.Backgroundcolor="White"
     if ($p.Parent.Parent) {. (Join-Path -Path (Split-Path -Parent -Path $PROFILE) -ChildPath 'Set-SolarizedLightColorDefaults.ps1') }
    }

	$bufferWidth = $host.ui.rawui.BufferSize.Width
	$bufferHeight = $host.ui.rawui.CursorPosition.Y
	$rec = new-object System.Management.Automation.Host.Rectangle 0,0,($bufferWidth - 1),$bufferHeight
	$buffer = $host.ui.rawui.GetBufferContents($rec)
        clear-host
	$YOffset = [console]::WindowTop
	 for($i = 0; $i -lt $bufferHeight; $i++)
	 {
	    $bufferLine = New-Object System.Text.StringBuilder
	    for($j = 0; $j -lt $bufferWidth; $j++)
	    {
	       $char = $buffer[$i,$j]
	       $null = $bufferLine.Append($char.Character) 
	    }
	    [console]::setcursorposition(0,$YOffset+$i)
	    Write-Host $bufferLine.ToString() -NoNewline
	 }

    if ($currentPrincipal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator ))
    {
        write-host "Warning: PowerShell is running as an Administrator.`n"
    }
}

VsVars32("17.0")
# Install-Module posh-git -Scope CurrentUser -Force
# $env:PATH = $env:PATH + ";C:\Program Files\Git\bin"

$env:PATH = $env:PATH + ";C:\Program Files\erl-23.2\bin;C:\Users\steve\Documents\GitHub\rebar"
$env:PATH = $env:PATH + ";C:\Program Files (x86)\Elixir\bin;C:\Users\steve\.mix\escripts"

Import-Module posh-git
# Install-Module WslInterop
Import-WslCommand "awk", "emacs", "grep", "head", "less", "ls", "man", "sed", "seq", "ssh", "tail"
