$notify = "hkcu:\Software\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\TrayNotify"
if (Get-ItemProperty -path $notify -name IconStreams -ErrorAction SilentlyContinue ) { remove-itemproperty -path $notify -name IconStreams }
if (Get-ItemProperty -path $notify -name PastIconStreams -ErrorAction SilentlyContinue ){ remove-itemproperty -path $notify -name PastIconStreams}
$explorer = get-process -Name explorer | Select-Object -First 1
$path = $explorer.Path
stop-process -Name explorer
start-process $path

