<# 
.SYNOPSIS 
    Stylecop HTML report generator. 
    
.DESCRIPTION 
    Creates *.html from *.StyleCop.xml in the current directory 
    These are reports with pretty reporting
        
.NOTES 
    File Name  : Transform-StyleCopReport.ps1 
    Requires   : PowerShell Version 2.0
#> 
function Insert-Newline([System.Xml.XmlElement] $element) {
    $break = $element.OwnerDocument.CreateSignificantWhitespace("`r`n");
    $element.AppendChild($break) | Out-Null
}

function Make-HTML([string] $file) {
    Write-Host "Processing report $file"
    $htmlfile = $file.Replace(".StyleCop.xml", ".html")
    [xml]$raw = Get-Content $file
    [xml]$report = @"
<!DOCTYPE html>
<html>
<head>
<title>StyleCop Report</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<style>
body {position: absolute; top: 0px; width: 100%;margin:0px;min-height:100%;padding:0}
body {font-family:Verdana, Helvetica, sans-serif;font-size:70%;background:#fff;color:#000;}
h1 { padding: 10px; width: 100%; background-color: #566077; color: #fff; }
h2 { margin:10px; padding: 10px; background-color: #ffc; border: #d7ce28 1px solid;  }
h3 { margin:25px; padding: 10px; background: #b9c9fe; color: #039; border: 1px solid #aabcfe; }
h3 span {float: right;}
div { margin:25px; border: #d7ce28 1px solid; }
div div, div.plain { border: none; }
pre { font-family: Consolas, Inconsolata, "Lucida Console", "Courier New", monospace;font-size:100%; padding: 0; margin:0}
pre.odd { background: #eee; }
pre.even { background: #ddf; }
pre.monospace { display:inline; }
span.violation { background: #ff8; }
span.index { border-right : 1px solid black }
span.fill { width:80em; display: inline-block; }
input { margin:25px; }
</style>
<script type="text/javascript">
`$(function(){
  // inner first, then outer
  `$('span.violation')
    .click(function(event) {
      if (this == event.target) {
        `$(this).parent().next().toggle();
      }
    return false;
  })
  .css('cursor', 'pointer')
  .click();  
  
  `$('h3')
    .click(function(event) {
      if (this == event.target) {
        `$(this).next().toggle();
      }
    return false;
  })
  .css('cursor', 'pointer')
  .click();
  
    `$("#expand").click(function(){
        `$('.toggle').show();
    });
    `$("#collapse").click(function(){
        `$('.toggle').hide();
    });
});
</script>

</head>

<body>
<h1>StyleCop Report</h1>

</body>

</html>
"@
    $body = (Select-Xml -Xml $report -XPath "//body").Node
    $doc = $body.OwnerDocument
    $doc.PreserveWhitespace = $true
    Insert-Newline $body
    
    $x = $doc.CreateElement("h2", "")
    $violations = @(Select-Xml -Xml $raw -XPath "//Violation")

    $x.InnerText = "Total Violations : $($violations.Length)"
    $body.appendChild($x) | Out-Null
    Insert-Newline $body
    
    $vfiles = @($violations | % { $_.Node.Source } | Sort-Object -Unique)
    
    $vfiles | % { 
        if (Test-Path $_) {
          $path = Resolve-Path $_
        } else {
          $path = $_
        }
        
        $local = @(Select-Xml -Xml $raw -XPath "//Violation[@Source='$_']")

        $h = $doc.CreateElement("h3", "")
        $body.AppendChild($h) | Out-Null
        Insert-Newline $body
        
        $t = $doc.CreateTextNode("SourceFile: $path")
        $span = $doc.CreateElement("span", "")
        
        $span.InnerText = "($($local.Length) violations)"
        $h.AppendChild($t)     | Out-Null
        $h.AppendChild($span)  | Out-Null
        
        $sdiv = $doc.CreateElement("div", "")
        $sdiv.SetAttribute("class", "toggle")
        $body.AppendChild($sdiv) | Out-Null
        
        $leftovers = @($local | % { $_.Node })
                    
        if (Test-Path $_) {
          $source = Get-Content $path
        
          $line = 0
          $source | % {
            $line += 1
            $pre = $doc.CreateElement("pre", "")
            $num = $line.ToString()
            while ($num.Length -lt 4) { $num = " " + $num }
            $span1 = $doc.CreateElement("span", "")
            $span1.InnerText = "$num "
            $span1.SetAttribute("class", "index")
            $pre.AppendChild($span1) | Out-Null
            
            $span2 = $doc.CreateElement("span", "")
            $span2.InnerText = $_
            $pre.AppendChild($span2) | Out-Null
            
            $sdiv.AppendChild($pre) | Out-Null
            $pre.SetAttribute("class", "odd")
            if (0 -eq ($line % 2)) { $pre.SetAttribute("class", "even") }
            
            $localhere = @($local | % { $_.Node } | ? { $_.LineNumber -eq $line })
            if ($localhere.Length -gt 0) { 
                if ($span2.InnerText) {
                    $span2.SetAttribute("class", "violation")
                } else {
                    $span2.SetAttribute("class", "violation fill")
                    $span2.InnerText = " "
                }
                $pdiv = $doc.CreateElement("div", "")
                $pdiv.SetAttribute("class", "toggle")
                Insert-Newline $sdiv
                $sdiv.AppendChild($pdiv) | Out-Null
                $ul = $doc.CreateElement("ul", "")
                $pdiv.AppendChild($ul) | Out-Null
                $localhere | % {
                    $li = $doc.CreateElement("li", "")
                    $li.InnerText = $_.InnerText
                    $ul.AppendChild($li) | Out-Null
                    Insert-Newline $ul
                } #$localhere
              } #if localhere
           } #$source
           
              $leftovers = @($leftovers | ? { 
                 $here = " $($_.LineNumber)"
                 $num = [int]$here
                 ($num -lt 1) -or ($num -gt $line)
              })
           } #if test-path
           

             $localhere = @($leftovers | % {
               $line = " $($_.LineNumber)"
               while ($line.Length -lt 4) { $line = " " + $line }
               $x = New-Object PSobject
               $x | Add-Member -MemberType NoteProperty -Name LineNumber -Value $line
               $x | Add-Member -MemberType NoteProperty -Name InnerText -Value $_.InnerText
               $x
             } | Sort-Object -Property LineNumber)
             $ul = $doc.CreateElement("ul", "")
             $sdiv.AppendChild($ul) | Out-Null
             $prevline = ""
             $localhere | % {
               $li = $doc.CreateElement("li", "")
               $pre = $doc.CreateElement("pre", "")
               $li.AppendChild($pre) | Out-Null
               $pre.SetAttribute("class", "monospace")
               $num = $_.LineNumber
               while ($num.Length -lt 4) { $num = " " + $num }
               $pre.InnerText = "Line $num | "
               if ($pre.InnerText -eq $prevline)
               {
                 $pre.InnerText = "          | "
               }
               else
               {
                 $prevline = $pre.InnerText
               }
               $span = $doc.CreateElement("span", "")
               $span.InnerText = $_.InnerText
               $li.AppendChild($span) | Out-Null
               $ul.AppendChild($li) | Out-Null
               Insert-Newline $ul
             } # $localhere

           Insert-Newline $sdiv
    } # $vfiles
    
    Insert-Newline $body
    $tdiv = $doc.CreateElement("div", "")
    $tdiv.SetAttribute("class", "plain")
    $body.AppendChild($tdiv) | Out-Null
    
    $expand = $doc.CreateElement("input", "")
    $expand.SetAttribute("type", "button")
    $expand.SetAttribute("id", "expand")
    $expand.SetAttribute("name", "expand")
    $expand.SetAttribute("value", "Expand All")
    $tdiv.AppendChild($expand) | Out-Null
    
    $collapse = $doc.CreateElement("input", "")
    $collapse.SetAttribute("type", "button")
    $collapse.SetAttribute("id", "collapse")
    $collapse.SetAttribute("name", "collapse")
    $collapse.SetAttribute("value", "Collapse All")
    $tdiv.AppendChild($collapse) | Out-Null
    
    $report.Save($htmlfile)
} # function

$files = @(dir *.StyleCop.xml)
$files | % { Make-HTML $_.FullName }